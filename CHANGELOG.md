# Changelog
## 1.1.8
### Changed
- Added maximum version constraint for Mypy to prevent type errors caused by
  Mypy updates.

## 1.1.7
### Fixed
- The REST API returns
  [UnspecifiedError](https://adralioh.gitlab.io/ytdl-server/api.html#unspecifiederror)
  if you try to create/cancel a job with the Content-Type set incorrectly when
  using Werkzeug 2.1 or higher.

  The expected behavior is for the REST API to return
  [DataBadType](https://adralioh.gitlab.io/ytdl-server/api.html#databadtype).
- Added maximum version constraint for Werkzeug because the supported Flask versions won't work with Werkzeug 3.
### Changed
- Bumped API version to 1.1.1

## 1.1.6
### Fixed
- Added maximum version constraint for SQLAlchemy because one of ytdl-server's
  dependencies doesn't support SQLAlchemy 2.

## 1.1.5
### Fixed
- Added maximum version constraint for Flask because ytdl-server doesn't work
  with Flask v2.2.0.

  Flask v2.1.3 is the latest working version.
- Mypy raises the following error when using Flask v2.1.3:
  ```
  ytdl_server/json.py:69: error: Value of type variable "ft.ErrorHandlerDecorator" of function cannot be "Callable[[HTTPException], Tuple[Response, int]]"  [type-var]
  ```

## 1.1.4
### Fixed
- The *parse_metadata* and *replace_in_metadata* custom_opts cause jobs to crash
  when using yt-dlp version 2022.05.18 or later.

## 1.1.3
### Fixed
- Mypy raises an error about unused `type: ignore` comments when using v0.950 or
  newer.

## 1.1.2
### Fixed
- Docker images fail to build due to a change in PycURL.

## 1.1.1
### Fixed
- Jobs randomly fail when the database connection uses SSL.

## 1.1.0
### Added
- A health check for the REST API. See
  [Health](https://adralioh.gitlab.io/ytdl-server/api.html#health) for
  documentation.

  The Docker image for the API has also been updated to use the health check.
- Redis and SQS dependencies to the Docker images. This allows you to use Redis
  or Amazon SQS as the Celery broker when using the Docker images.
- Documentation about ytdl-server's versioning scheme. See
  [Versioning scheme](https://adralioh.gitlab.io/ytdl-server/version.html).
### Changed
- Bumped API version to 1.1.0
### Fixed
- Improved database load when running jobs by reducing the number of
  transactions that are created.

## 1.0.2
### Fixed
- Slow video download speed caused by database I/O bottleneck.

## 1.0.1
### Fixed
- The *parse_metadata* and *replace_in_metadata* custom_opts cause the job to
  crash.
