ARG PYTHON_VERSION=3-slim-bullseye

# Build psycopg2 and pycurl
# pycurl~=7.44.1 is required by celery[sqs]
# psycopg2 reference: https://www.psycopg.org/docs/install.html
################################################################################
FROM python:$PYTHON_VERSION as build

RUN apt-get update &&\
    apt-get install -y --no-install-recommends \
        libpq-dev gcc libc-dev libcurl4-openssl-dev libssl-dev &&\
    rm -rf /var/lib/apt/lists/*

WORKDIR /build
RUN pip wheel --no-cache-dir --no-binary :all: psycopg2 'pycurl~=7.44.1'


# Common directives shared between the REST API and the Celery worker
################################################################################
FROM python:$PYTHON_VERSION as common

WORKDIR /usr/src/ytdl-server

COPY --from=build /build/*.whl /build/
COPY requirements.txt ./

# UID of the user that will be used to run the images
ARG UID=235000

# celery[redis] and celery[sqs] are installed so that Celery can use them as a
#   broker. By default, only RabbitMQ is usable
# libpq5 is required by psycopg2
# curl is required by pycurl, which is required by celery[sqs]
# The user's group is created manually because the GID won't automatically match
#   the UID if it's above UID_MAX (60000)
RUN apt-get update &&\
    apt-get install -y --no-install-recommends libpq5 curl &&\
    pip install --no-cache-dir /build/*.whl &&\
    pip install --no-cache-dir -r requirements.txt &&\
    pip install --no-cache-dir 'celery[redis,sqs]' &&\
    rm -rf /var/lib/apt/lists/* &&\
    rm -rf /build &&\
    groupadd --gid "$UID" ytdl &&\
    useradd --uid "$UID" --gid ytdl --no-create-home --home-dir=/ \
      --comment 'ytdl-server User' --shell /usr/sbin/nologin ytdl

COPY ytdl_server ./ytdl_server/
COPY config.yml /config/ytdl-server.yml

# Name of the youtube-dl module that will be imported
ARG YTDL_MODULE=youtube_dl

ENV YTDL_CONFIG=/config/ytdl-server.yml \
    YTDL_MODULE=${YTDL_MODULE}

VOLUME /config


# Flask REST API with Gunicorn
################################################################################
FROM common AS api

RUN pip install --no-cache-dir gunicorn

HEALTHCHECK CMD curl --fail http://localhost:8000/health || exit 1

EXPOSE 8000/tcp
USER ytdl

CMD ["gunicorn", "--bind=0.0.0.0:8000", "--workers=4", "ytdl_server.flask:with_logging()"]


# Celery worker
################################################################################
FROM common AS worker

# Name of the youtube-dl package that will be installed from PyPI.
# Defaults to the same value as YTDL_MODULE
ARG YTDL_PACKAGE=${YTDL_MODULE}
# Whether or not to install ffmpeg. Set to any value to enable
ARG INSTALL_FFMPEG

RUN pip install --no-cache-dir -- "$YTDL_PACKAGE" &&\
    mkdir -p /videos /cache &&\
    chown ytdl: /videos /cache &&\
    chmod 755 /videos &&\
    chmod 700 /cache &&\
    if [ -n "$INSTALL_FFMPEG" ]; then \
      apt-get update &&\
      apt-get install -y --no-install-recommends ffmpeg &&\
      rm -rf /var/lib/apt/lists/* ;\
    fi

VOLUME /videos /cache
USER ytdl

CMD ["celery", "--app=ytdl_server.run_celery", "worker"]
