#############
Configuration
#############

Configuration can be supplied via a config file and via environment variables.

***********
Config file
***********
A YAML config file can be supplied by setting the ``YTDL_CONFIG`` env-var to
the path of the file.

If you're using the `Docker images`_, the config file is located at
``/config/ytdl-server.yml``.

.. _Docker images: https://gitlab.com/adralioh/ytdl-server#registry

See `List of config options`_ for each option's corresponding config key.

.. highlight:: yaml

Example config file::

    ---
    database_uri: postgresql://user:password@db.example.com/ytdl
    download_dir: /videos

    ytdl_opts:
      default_opts:
        format: bestvideo+bestaudio/best
        outtmpl: '%(title)s-%(id)s.%(ext)s'
        writesubtitles: true
        allsubtitles: true

    custom_opts:
      default_opts:
        addmetadata: true
        embedsubtitles: true

    celery_config:
      broker_url: amqp://broker.example.com

*********************
Environment variables
*********************
Some options can be supplied via environment variables.

See `List of config options`_ for each option's corresponding env-var.

Env-vars take priority over the config file.

Boolean environment variables
=============================
Boolean env-vars accept the following values:

:True: ``True``, ``T``, ``1``, ``Yes``, ``Y``
:False: ``False``, ``F``, ``0``, ``No``, ``N``

Case is ignored.

List environment variables
==========================
List env-vars accept a comma-separated list of strings.

For example, ``foo,bar,fizz`` expands to ``['foo', 'bar', 'fizz']``.

.. _config-options:

**********************
List of config options
**********************

database_uri
============
:Config key: ``database_uri``
:Env var: ``YTDL_DATABASE_URI``
:Type: String
:Default: **Required**

URI of the PostgreSQL database, in the format
:samp:`postgresql://{username}:{password}@{host}:{port}/{database}`

flask_config
============
:Config key: ``flask_config``
:Env var: See `Flask environment variables`_
:Type: Dictionary
:Default: ``{}``

Flask configuration options.

See the `Flask documentation`_ for a list of options.

.. _Flask documentation: https://flask.palletsprojects.com/en/2.0.x/config/#builtin-configuration-values

.. _celery_config:

celery_config
=============
:Config key: ``celery_config``
:Env var: See `Celery environment variables`_
:Type: Dictionary
:Default: ``{}``

Celery configuration options.

See the `Celery documentation`_ for a list of options.

.. _Celery documentation: https://docs.celeryproject.org/en/stable/userguide/configuration.html#configuration-directives

.. [#result_backend] The *result_backend* option is automatically set to
   :samp:`db+{database_uri}` (database_uri_) if it's not explicitly given.

download_dir
============
:Config key: ``download_dir``
:Env var: ``YTDL_DOWNLOAD_DIR``
:Type: String
:Default: The working directory of the REST API

Default directory that videos will be downloaded to.

Users can override this via the *outtmpl* youtube-dl option if
force_download_dir_ is ``False``.

force_download_dir
==================
:Config key: ``force_download_dir``
:Env var: ``YTDL_FORCE_DOWNLOAD_DIR``
:Type: Boolean
:Default: ``True``

Whether or not to force videos to be downloaded to download_dir_.

If the user attempts to download to a location outside of the download
directory, an error will be raised.

.. _allow_user_opts:

allow_user_opts
===============
:Config key: ``allow_user_opts``
:Env var: ``YTDL_ALLOW_USER_OPTS``
:Type: Boolean
:Default: ``True``

Whether or not to allow the user to pass their own youtube-dl options in
addition to the options specified in ytdl_default_opts_ and
custom_default_opts_.

The options that users are allowed to pass can be restricted via
ytdl_whitelist_ and custom_whitelist_.

.. _ytdl_default_opts:

ytdl_default_opts
=================
:Config key: ``ytdl_opts.default_opts``
:Env var: None
:Type: Dictionary
:Default: ``{}``

Default :ref:`ytdl_opts` that will be passed to youtube-dl.

See `Config file`_ for an example.

These options can be overridden by the user if allow_user_opts_ is ``True`` and
the option is whitelisted via ytdl_whitelist_.

.. _ytdl_enable_whitelist:

ytdl_enable_whitelist
=====================
:Config key: ``ytdl_opts.enable_whitelist``
:Env var: ``YTDL_ENABLE_WHITELIST``
:Type: Boolean
:Default: ``True``

Whether or not to enable ytdl_whitelist_.

.. warning::
   Disabling the whitelist is not recommended because some of the
   options (such as *postprocessors*) allow for arbitrary code execution.

This has no effect if allow_user_opts_ is ``False``.

.. _ytdl_whitelist:

ytdl_whitelist
==============
:Config key: ``ytdl_opts.whitelist``
:Env var: ``YTDL_WHITELIST``
:Type: List
:Default: ``[
          'age_limit', 'allsubtitles', 'ap_mso', 'ap_password', 'ap_username',
          'buffersize', 'continuedl', 'default_search', 'fixup',
          'force_generic_extractor', 'format', 'geo_bypass',
          'geo_bypass_country', 'geo_bypass_ip_block', 'hls_use_mpegts',
          'http_chunk_size', 'ignoreerrors', 'include_ads', 'keepvideo',
          'matchtitle', 'max_filesize', 'max_sleep_interval', 'max_views',
          'merge_output_format', 'min_filesize', 'min_views', 'nooverwrites',
          'nopart', 'noplaylist', 'noresizebuffer', 'outtmpl',
          'outtmpl_na_placeholder', 'password', 'playlistend',
          'playlist_items', 'playlistrandom', 'playlistreverse',
          'playliststart', 'ratelimit', 'rejecttitle', 'restrictfilenames',
          'retries', 'simulate', 'skip_download', 'sleep_interval',
          'subtitlesformat', 'subtitleslangs', 'updatetime', 'username',
          'videopassword', 'write_all_thumbnails', 'writeannotations',
          'writeautomaticsub', 'writedescription', 'writeinfojson',
          'writesubtitles', 'writethumbnail', 'xattr_set_filesize',
          'youtube_include_dash_manifest',
          'allow_multiple_audio_streams', 'allow_multiple_video_streams',
          'allow_playlist_files', 'break_on_existing', 'break_on_reject',
          'check_formats', 'dynamic_mpd', 'extractor_retries',
          'force_write_download_archive', 'final_ext', 'format_sort',
          'format_sort_force', 'fragment_retries', 'getcomments',
          'hls_split_discontinuity', 'ignore_no_formats_error', 'overwrites',
          'skip_playlist_after_errors', 'sleep_interval_requests',
          'sleep_interval_subtitles', 'throttledratelimit', 'trim_file_name',
          'windowsfilenames', 'writedesktoplink', 'writeurllink',
          'writewebloclink', 'youtube_include_hls_manifest'
          ]``

List of youtube-dl options that the user is allowed to pass via the *ytdl_opts*
key when creating a job.

If you just want to add or remove individual options, see ytdl_whitelist_add_
and ytdl_whitelist_remove_.

The later options (starting with *allow_multiple_audio_streams*) require
yt-dlp_.

This has no effect if allow_user_opts_ and/or ytdl_enable_whitelist_ is
``False``.

.. _ytdl_whitelist_add:

ytdl_whitelist_add
==================
:Config key: ``ytdl_opts.whitelist_add``
:Env var: ``YTDL_WHITELIST_ADD``
:Type: List
:Default: ``[]``

List of additional youtube-dl options that will be added to ytdl_whitelist_.

This allows you to add individual options without redefining the entire list.

.. _ytdl_whitelist_remove:

ytdl_whitelist_remove
=====================
:Config key: ``ytdl_opts.whitelist_remove``
:Env var: ``YTDL_WHITELIST_REMOVE``
:Type: List
:Default: ``[]``

List of youtube-dl options that will be removed from ytdl_whitelist_.

This allows you to remove individual options without redefining the entire
list.

ytdl_sensitive_opts
===================
:Config key: ``ytdl_opts.sensitive_opts``
:Env var: ``YTDL_SENSITIVE_OPTS``
:Type: List
:Default: ``['ap_password', 'password', 'videopassword']``

List of youtube-dl options that will be censored when displayed to the user.

This prevents passwords from being exposed.

.. _custom_default_opts:

custom_default_opts
===================
:Config key: ``custom_opts.default_opts``
:Env var: None
:Type: Dictionary
:Default: ``{}``

Default :doc:`custom_opts` that will be used when creating a job.

See `Config file`_ for an example.

These options can be overridden by the user if allow_user_opts_ is ``True`` and
the option is whitelisted via custom_whitelist_.

.. _custom_enable_whitelist:

custom_enable_whitelist
=======================
:Config key: ``custom_opts.enable_whitelist``
:Env var: ``YTDL_ENABLE_CUSTOM_WHITELIST``
:Type: Boolean
:Default: ``False``

Whether or not to enable custom_whitelist_.

This has no effect if allow_user_opts_ is ``False``.

custom_whitelist
================
:Config key: ``custom_opts.whitelist``
:Env var: ``YTDL_CUSTOM_WHITELIST``
:Type: List
:Default: ``[]``

List of custom youtube-dl options that the user is allowed to pass via the
:doc:`custom_opts` field when creating a job.

This has no effect if allow_user_opts_ and/or custom_enable_whitelist_ is
``False``.

.. _ytdl_module:

ytdl_module
===========
:Config key: ``ytdl_module``
:Env var: ``YTDL_MODULE``
:Type: String
:Default: ``youtube_dl``

The name of the youtube-dl module that ytdl_class_ will be imported from.

Change this if you want to use a fork of youtube-dl that has a different
module name

For example, set this to ``yt_dlp`` if you want to use yt-dlp_.

.. _yt-dlp: https://github.com/yt-dlp/yt-dlp

.. _ytdl_class:

ytdl_class
==========
:Config key: ``ytdl_class``
:Env var: ``YTDL_CLASS``
:Type: String
:Default: ``YoutubeDL``

The name of the youtube-dl class that will be used to download videos.

The class is imported from ytdl_module_.

daterange_module
================
:Config key: ``daterange_module``
:Env var: ``YTDL_DATERANGE_MODULE``
:Type: String
:Default: Same value as ytdl_module_

The name of the youtube-dl module that daterange_class_ will be imported from.

You don't normally need to change this since it inherits the value from
ytdl_module_.

daterange_class
===============
:Config key: ``daterange_class``
:Env var: ``YTDL_DATERANGE_CLASS``
:Type: String
:Default: ``DateRange``

The name of the DateRange class that will be used for the
:ref:`datebefore and dateafter <daterange>` custom_opts.

The class is imported from daterange_module_.

metadata_module
===============
:Config key: ``metadata_module``
:Env var: ``YTDL_METADATA_MODULE``
:Type: String
:Default: Same value as ytdl_module_

The name of the youtube-dl module that metadata_class_ will be imported from.

You don't normally need to change this since it inherits the value from
ytdl_module_.

metadata_class
==============
:Config key: ``metadata_class``
:Env var: ``YTDL_METADATA_CLASS``
:Type: String
:Default: ``MetadataParserPP``

The name of the MetadataParser postprocessor class that will be used for the
:ref:`parse_metadata` and :ref:`replace_in_metadata` custom_opts.

The class is imported from metadata_module_.

.. _YTDL_LOG_LEVEL:

YTDL_LOG_LEVEL
==============
:Config key: None
:Env var: ``YTDL_LOG_LEVEL``
:Type: String
:Default: ``WARNING``

Set the logging verbosity level of the application.

Possible values (case insensitive): ``DEBUG``, ``INFO``, ``WARNING``, ``ERROR``,
``CRITICAL``

***************************
Flask environment variables
***************************
Many Flask options can be specified via env-vars.

See the `Flask documentation`_ for information about each option.

If you need to pass an option that isn't listed here, it must be passed via the
config file. See `flask_config`_.

.. csv-table:: Options
   :header: Name, Type, Env-var

   SERVER_NAME, String, YTDL_FLASK_SERVER_NAME
   APPLICATION_ROOT, String, YTDL_FLASK_APPLICATION_ROOT
   PREFERRED_URL_SCHEME, String, YTDL_FLASK_PREFERRED_URL_SCHEME
   MAX_CONTENT_LENGTH, Integer, YTDL_FLASK_MAX_CONTENT_LENGTH
   JSON_AS_ASCII, Boolean, YTDL_FLASK_JSON_AS_ASCII
   JSON_SORT_KEYS, Boolean, YTDL_FLASK_JSON_SORT_KEYS
   JSONIFY_PRETTYPRINT_REGULAR, Boolean, YTDL_FLASK_JSONIFY_PRETTYPRINT_REGULAR
   JSONIFY_MIMETYPE, String, YTDL_FLASK_JSONIFY_MIMETYPE


****************************
Celery environment variables
****************************
Many Celery options can be specified via env-vars.

See the `Celery documentation`_ for information about each option.

If you need to pass an option that isn't listed here, it must be passed via the
config file. See `celery_config`_.

.. csv-table:: Options
   :header: Name, Type, Env-var

   enable_utc, Boolean, YTDL_CELERY_ENABLE_UTC
   timezone, String, YTDL_CELERY_TIMEZONE
   task_compression, String, YTDL_CELERY_TASK_COMPRESSION
   task_protocol, Integer, YTDL_CELERY_TASK_PROTOCOL
   task_publish_retry, Boolean, YTDL_CELERY_TASK_PUBLISH_RETRY
   task_soft_time_limit, Integer, YTDL_CELERY_TASK_SOFT_TIME_LIMIT
   task_acks_late, Boolean, YTDL_CELERY_TASK_ACKS_LATE
   task_acks_on_failure_or_timeout, Boolean, YTDL_CELERY_TASK_ACKS_ON_FAILURE_OR_TIMEOUT
   task_reject_on_worker_lost, Boolean, YTDL_CELERY_TASK_REJECT_ON_WORKER_LOST
   task_default_rate_limit, Integer, YTDL_CELERY_TASK_DEFAULT_RATE_LIMIT
   result_backend [#result_backend]_, String, YTDL_CELERY_RESULT_BACKEND
   result_backend_always_retry, Boolean, YTDL_CELERY_RESULT_BACKEND_ALWAYS_RETRY
   result_backend_max_sleep_between_retries_ms, Integer, YTDL_CELERY_RESULT_BACKEND_MAX_SLEEP_BETWEEN_RETRIES_MS
   result_backend_base_sleep_between_retries_ms, Integer, YTDL_CELERY_RESULT_BACKEND_BASE_SLEEP_BETWEEN_RETRIES_MS
   result_backend_max_retries, Integer, YTDL_CELERY_RESULT_BACKEND_MAX_RETRIES
   result_compression, String, YTDL_CELERY_RESULT_COMPRESSION
   result_expires, Integer, YTDL_CELERY_RESULT_EXPIRES
   result_chord_join_timeout, Float, YTDL_CELERY_RESULT_CHORD_JOIN_TIMEOUT
   result_chord_retry_interval, Float, YTDL_CELERY_RESULT_CHORD_RETRY_INTERVAL
   database_short_lived_sessions, Boolean, YTDL_CELERY_DATABASE_SHORT_LIVED_SESSIONS
   broker_url, String, YTDL_CELERY_BROKER_URL
   broker_read_url, String, YTDL_CELERY_BROKER_READ_URL
   broker_write_url, String, YTDL_CELERY_BROKER_WRITE_URL
   broker_failover_strategy, String, YTDL_CELERY_BROKER_FAILOVER_STRATEGY
   broker_heartbeat, Float, YTDL_CELERY_BROKER_HEARTBEAT
   broker_heartbeat_checkrate, Float, YTDL_CELERY_BROKER_HEARTBEAT_CHECKRATE
   broker_pool_limit, Integer, YTDL_CELERY_BROKER_POOL_LIMIT
   broker_connection_timeout, Float, YTDL_CELERY_BROKER_CONNECTION_TIMEOUT
   broker_connection_retry, Boolean, YTDL_CELERY_BROKER_CONNECTION_RETRY
   broker_connection_max_retries, Integer, YTDL_CELERY_BROKER_CONNECTION_MAX_RETRIES
   broker_login_method, String, YTDL_CELERY_BROKER_LOGIN_METHOD
   worker_concurrency, Integer, YTDL_CELERY_WORKER_CONCURRENCY
   worker_prefetch_multiplier, Integer, YTDL_CELERY_WORKER_PREFETCH_MULTIPLIER
   worker_lost_wait, Float, YTDL_CELERY_WORKER_LOST_WAIT
   worker_max_tasks_per_child, Integer, YTDL_CELERY_WORKER_MAX_TASKS_PER_CHILD
   worker_max_memory_per_child, Integer, YTDL_CELERY_WORKER_MAX_MEMORY_PER_CHILD
   worker_disable_rate_limits, Boolean, YTDL_CELERY_WORKER_DISABLE_RATE_LIMITS
   worker_state_db, String, YTDL_CELERY_WORKER_STATE_DB
   worker_timer_precision, Float, YTDL_CELERY_WORKER_TIMER_PRECISION
   worker_enable_remote_control, Boolean, YTDL_CELERY_WORKER_ENABLE_REMOTE_CONTROL
   worker_proc_alive_timeout, Float, YTDL_CELERY_WORKER_PROC_ALIVE_TIMEOUT
   worker_cancel_long_running_tasks_on_connection_loss, Boolean, YTDL_CELERY_WORKER_CANCEL_LONG_RUNNING_TASKS_ON_CONNECTION_LOSS
   worker_send_task_events, Boolean, YTDL_CELERY_WORKER_SEND_TASK_EVENTS
   task_send_sent_event, Boolean, YTDL_CELERY_TASK_SEND_SENT_EVENT
   event_queue_ttl, Float, YTDL_CELERY_EVENT_QUEUE_TTL
   event_queue_expires, Float, YTDL_CELERY_EVENT_QUEUE_EXPIRES
   event_queue_prefix, String, YTDL_CELERY_EVENT_QUEUE_PREFIX
   event_exchange, String, YTDL_CELERY_EVENT_EXCHANGE
   control_queue_ttl, Float, YTDL_CELERY_CONTROL_QUEUE_TTL
   control_queue_expires, Float, YTDL_CELERY_CONTROL_QUEUE_EXPIRES
   control_exchange, String, YTDL_CELERY_CONTROL_EXCHANGE
   worker_log_color, Boolean, YTDL_CELERY_WORKER_LOG_COLOR
   worker_log_format, String, YTDL_CELERY_WORKER_LOG_FORMAT
   worker_task_log_format, String, YTDL_CELERY_WORKER_TASK_LOG_FORMAT
   beat_schedule_filename, String, YTDL_CELERY_BEAT_SCHEDULE_FILENAME
   beat_sync_every, Integer, YTDL_CELERY_BEAT_SYNC_EVERY
   beat_max_loop_interval, Integer, YTDL_CELERY_BEAT_MAX_LOOP_INTERVAL
