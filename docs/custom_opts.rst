###########
custom_opts
###########
The custom_opts field contains special youtube-dl options that can't be given
directly via :ref:`ytdl_opts`. These options are converted to regular ytdl_opts
when running a job.

When :ref:`creating a job <create>`, the user-supplied custom_opts are merged
into :ref:`custom_default_opts`. If the same option is given in both the
user-supplied custom_opts and :ref:`custom_default_opts`, the user-supplied
value will be used.

When :ref:`getting the status of a job <get>`, custom_opts is merged into
:ref:`ytdl_opts`.

.. _daterange:

*********************
datebefore, dateafter
*********************
:Type: String

These options are used to only download videos before or after the given date
(inclusive).

Equivalent to the ``--datebefore`` and ``--dateafter`` youtube-dl
command-line arguments.

Possible values:

- A date in the format ``YYYYMMDD``
- ``now``, ``today``, or ``yesterday``
- A relative date that matches the following regex::

      (now|today)[+-][0-9]+(day|week|month|year)(s)?

  Example: ``today-2weeks`` (two weeks ago)

************
sponsorblock
************
:Type: Object

.. note::
   Requires yt-dlp

Make chapter entries for, or remove various segments (sponsor, introductions,
etc.) from downloaded YouTube videos using the SponsorBlock API.

The value should be an object with the following fields:

:mark (array of strings): List of SponsorBlock categories to create chapters
                          for. You can prefix a category with ``-`` to exempt
                          it.
:remove (array of strings): List of SponsorBlock categories to remove from the
                            video.
:template (string): Title template for chapters created via SponsorBlock.
                    Optional; the default yt-dlp value will be used if not
                    given.
:api (string): SponsorBlock API URL. Optional; the default yt-dlp value will be
               used if not given.
:force_keyframes (boolean): Whether or not to require keyframes around the
                            chapters when removing them. Requires a re-encode
                            and thus is very slow, but the resulting video may
                            have fewer artifacts around the cuts. If
                            remove_chapters_ is also given, the value will
                            override the one defined here.
                            Default: *false*.

At least one of *mark* or *remove* must be given.

Available categories: *all*, *sponsor*, *intro*, *outro*, *selfpromo*,
*interaction*, *preview*, *music_offtopic*

See the `SponsorBlock wiki`_ for information about the categories.

.. _SponsorBlock wiki: https://wiki.sponsor.ajay.app/w/Segment_Categories

.. highlight:: json

Example::

  {
    "custom_opts": {
      "sponsorblock": {
        "mark": ["sponsor", "intro", "outro"],
        "remove": ["sponsor"],
        "template": "[SponsorBlock]: %(category_names)l",
        "api": "https://sponsor.ajay.app"
      }
    }
  }

.. _parse_metadata:

**************
parse_metadata
**************
:Type: Array of objects

.. note::
   Requires yt-dlp

Parse additional metadata like title/artist from other fields.

Equivalent to the ``--parse-metadata`` yt-dlp command-line argument.

See `Modifying Metadata`_ for information about the format.

The value should be an array of objects, and each object should have the
following fields:

:from (string): Source to extract data from. This can either be the name of
                a metadata field, or an output template.
:to (string): Regex or output template to interpret the matched field as.

.. _Modifying Metadata: https://github.com/yt-dlp/yt-dlp/#modifying-metadata

.. highlight:: json

Example::

  {
    "custom_opts": {
      "parse_metadata": [
        {
          "from": "title",
          "to": "%(artist)s - %(title)s",
        },
        {
          "from": "description",
          "to": "Artist - (?P<artist>.+)"
        }
      ]
    }
  }

.. _replace_in_metadata:

*******************
replace_in_metadata
*******************
:Type: Array of objects

.. note::
   Requires yt-dlp

Replace text in metadata fields using the given regular expressions.

Equivalent to the ``--replace-in-metadata`` yt-dlp command-line argument.

See `Modifying Metadata`_ for information about the format.

The value should be an array of objects, and each object should have the
following fields:

:fields (array of strings): List of metadata fields to replace text in.
:regex (string): Regex that will be used to match the text that should be
                 replaced.
:replace (string): Text that the matching regex will be replaced with.

.. highlight:: json

Example::

  {
    "custom_opts": {
      "replace_in_metadata": [
        {
          "fields": ["title", "uploader"],
          "regex": "[ _]",
          "replace": "-"
        }
      ]
    }
  }

*************
metafromtitle
*************
:Type: String

Parse additional metadata like song title / artist from the video title.

Equivalent to the ``--metadata-from-title`` youtube-dl command-line argument.

The format syntax is the same as the ``outtmpl`` ytdl_opt.
Regular expression with named capture groups may also be used. The parsed
parameters replace existing values.

Example: ``%(artist)s - %(title)s``

Example (regex): ``(?P<artist>.+?) - (?P<title>.+)``

****************
convertsubtitles
****************
:Type: String

Convert the subtitles to another format.

Equivalent to the ``--convert-subs`` youtube-dl command-line argument.

Example values: *srt*, *ass*, *vtt*, *lrc*

*****************
convertthumbnails
*****************
:Type: String

.. note::
   Requires yt-dlp

Convert the thumbnails to another format.

Equivalent to the ``--convert-thumbnails`` yt-dlp command-line argument.

Example values: *jpg*, *png*

************
extractaudio
************
:Type: Object

Convert video files to audio-only files.

Equivalent to the ``--extract-audio`` youtube-dl command-line argument.

The value should be an object with the following optional fields:

:audioformat (string): Audio format to use. Example values: *best*, *aac*,
                      *flac*, *mp3*, *m4a*, *opus*, *vorbis*, *wav*.
                      Default: *best*
:audioquality (integer): Audio quality. This can either be a value between
                        *0* (better) and *9* (worse) for VBR, or a specific
                        bitrate like *128* for CBR. Default: *5*
:nopostoverwrites (boolean): Do not overwrite post-processed files.
                          Default: *false*

.. highlight:: json

Example::

  {
    "custom_opts": {
      "extractaudio": {
        "audioformat": "mp3",
        "audioquality": 320
      }
    }
  }

**********
remuxvideo
**********
:Type: String

.. note::
   Requires yt-dlp

Remux the video into another container if necessary.

Equivalent to the ``--remux-video`` yt-dlp command-line argument.

You can specify multiple rules. For example, *aac>m4a/mov>mp4/mkv* will remux
*aac* to *m4a*, *mov* to *mp4*, and anything else to *mkv*.

Example values: *mp4*, *mkv*, *flv*, *webm*, *mov*, *avi*, *mp3*, *mka*, *m4a*,
*ogg*, *opus*

This option is incompatible with recodevideo_. If you supply both at once, this
option will be ignored.

***********
recodevideo
***********
:Type: String

Encode the video to another format if necessary.

Equivalent to the ``--recode-video`` youtube-dl command-line argument.

If you're using yt-dlp, the multiple-rule syntax as described in remuxvideo_ is
also supported.

Example values: *mp4*, *flv*, *ogg*, *webm*, *mkv*, *avi*

**************
embedsubtitles
**************
:Type: Boolean

If *True*, embed subtitles in the video if the video format supports it.

Equivalent to the ``--embed-subs`` youtube-dl command-line argument.

***************
remove_chapters
***************
:Type: Object

.. note::
   Requires yt-dlp

Remove chapters from the video that match the given regular expressions or time
ranges.

Equivalent to the ``--remove-chapters`` yt-dlp command-line argument.

The value should be an object with the following fields:

:patterns (array of strings): List of regular expressions. Any chapters that
                              match any of these expressions will be removed.
:ranges (array of number pairs): List of time ranges in seconds. Chapters that
                                 are within any of the time ranges will be
                                 removed.
:force_keyframes (boolean): Whether or not to require keyframes around the
                            chapters when removing them. Requires a re-encode
                            and thus is very slow, but the resulting video may
                            have fewer artifacts around the cuts.
                            Default: *false*.

At least one of *patterns* or *ranges* must be given.

.. highlight:: json

Example::

  {
    "custom_opts": {
      "remove_chapters": {
        "patterns": ["intro"],
        "ranges": [
          [0, 120], [600, 614.5]
        ]
        "force_keyframes": false
      }
    }
  }

***********
addmetadata
***********
:Type: Boolean

If *True*, write metadata to the video file.

Equivalent to the ``--add-metadata`` youtube-dl command-line argument.

***********
addchapters
***********
:Type: Boolean

.. note::
   Requires yt-dlp

If *True*, add chapter markers to the video file.

Equivalent to the ``--add-chapters`` yt-dlp command-line argument.

This will be automatically enabled if addmetadata_ is *True* or if the *mark*
field is given in sponsorblock_. You can override this by explicitly setting it
to *False*.

**************
embedthumbnail
**************
:Type: Boolean

If *True*, embed the thumbnail in the audio as cover art.

Equivalent to the ``--embed-thumbnail`` youtube-dl command-line argument.

**************
split_chapters
**************
:Type: Object

.. note::
   Requires yt-dlp

Split video into multiple files based on internal chapters.

Equivalent to the ``--split-chapters`` yt-dlp command-line argument.

The value should be an object with the following optional field:

:force_keyframes (boolean): Whether or not to require keyframes around the
                            chapters when removing them. Requires a re-encode
                            and thus is very slow, but the resulting video may
                            have fewer artifacts around the cuts.
                            Default: *false*.

.. highlight:: json

Example::

  {
    "custom_opts": {
      "split_chapters": {
        "force_keyframes": false
      }
    }
  }

******
xattrs
******
:Type: Boolean

If *True*, write metadata to the video file's xattrs (using Dublin Core and
XDG standards).

Equivalent to the ``--xattrs`` youtube-dl command-line argument.

