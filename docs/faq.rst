##########################
Frequently asked questions
##########################

.. contents:: Questions
   :depth: 1
   :local:
   :backlinks: none

I get an error saying that an option is forbidden when creating a job.
**********************************************************************
By default, only whitelisted :ref:`ytdl_opts` can be passed when creating jobs.
This prevents the user from passing options that can potentially cause issues.
For example, the *postprocessors* ytdl_opt allows for arbitrary code execution.

Possible solutions:

* Some ytdl_opts that aren't whitelisted by default can be passed safely via
  custom_opts instead. See :doc:`custom_opts`.

* The server default options aren't restricted by the whitelist. See
  :ref:`ytdl_default_opts`.

* If you're sure that the option is safe, you can add it to the whitelist. See
  :ref:`ytdl_whitelist_add`. Please also consider `opening an issue`_ if you
  think the option should be whitelisted by default.

  .. _opening an issue: https://gitlab.com/adralioh/ytdl-server/-/issues

* If you want to disable the whitelist completely, see
  :ref:`ytdl_enable_whitelist`.

  .. warning::
     Do not disable the whitelist unless the API is only accessible by trusted
     users.

.. note::
   It's also possible to use a whitelist for :doc:`custom_opts`, but it's not
   enabled by default because all of the options are safe to use. See
   :ref:`custom_enable_whitelist` if you want to enable it.

I don't know what ytdl_opt or custom_opt to use.
************************************************
See YoutubeDL_ for a list of available ytdl_opts.

.. note::
   If you're using a fork of youtube-dl, be sure to check your fork's
   documentation instead since many of them add extra ytdl_opts.

.. _YoutubeDL: https://github.com/ytdl-org/youtube-dl/blob/a8035827177d6b59aca03bd717acb6a9bdd75ada/youtube_dl/YoutubeDL.py#L141-L323

See :doc:`custom_opts` for a list of available custom_opts.

.. highlight:: bash

If you're used to running youtube-dl on the command line and just want to figure
out how to use a certain command-line argument, you can use ytcl_. Create a job
with the verbose flag (``-v``) and check the output to see what
ytdl_opt/custom_opt it translates to::

    $ ytcl -v create -f 'bestaudio/best' --extract-audio 'https://youtu.be/dQw4w9WgXcQ'
    ...
    [DEBUG] HTTP request: Method=POST | URL='http://localhost:8000/jobs/' | Data={'urls': ('https://youtu.be/dQw4w9WgXcQ',), 'ytdl_opts': {'format': 'bestaudio/best'}, 'custom_opts': {'extractaudio': {'audioformat': 'best', 'audioquality': 5, 'nopostoverwrites': False}}}
    ...

.. _ytcl: https://gitlab.com/adralioh/ytcl

How can I use a fork of youtube-dl?
***********************************
If the fork has the same module and class names as youtube-dl, it should be used
automatically if installed.

For forks like yt-dlp_ that use a different name, you can set the
:ref:`ytdl_module` and :ref:`ytdl_class` config options.

.. _yt-dlp: https://github.com/yt-dlp/yt-dlp

For example, if you want to use yt-dlp_, set the *YTDL_MODULE* env-var to
``yt_dlp``.

.. note::
   If you're using the Docker images, images are provided that use yt-dlp_ by
   default. See Registry_.

.. _Registry: https://gitlab.com/adralioh/ytdl-server#registry

I get an error saying that ffprobe/avprobe and ffmpeg/avconv aren't found.
**************************************************************************
If you're using the Docker images, you need to use an image that has FFmpeg
included. See Registry_.

If you're not using the Docker images, install FFmpeg and make sure that it's
available in the PATH.

How can I set a time limit for running jobs?
********************************************
You can set a time limit for jobs by setting the *task_soft_time_limit* Celery
config option. See :ref:`celery_config`.

The option can be set via the config file, or by setting the env-var
*YTDL_CELERY_TASK_SOFT_TIME_LIMIT*.

When a job reaches the time limit, the youtube-dl process will be killed, and
the :ref:`status` will be set to *timeout*.

How can I change the log level of ytdl-server?
**********************************************
You can change the log level via the :ref:`YTDL_LOG_LEVEL` env-var.

How do I change the URL path that the REST API is hosted at?
************************************************************
You can change the URL prefix (path) that the REST API is hosted at by setting
the *SCRIPT_NAME* variable on your WSGI server.

Many WSGI servers (including Gunicorn, which is used by the Docker images) allow
you to set this via the *SCRIPT_NAME* env-var.

For example, if you want the REST API to listen at ``example.com/api``, set the
*SCRIPT_NAME* env-var to ``/api``.
