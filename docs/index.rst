#########################
ytdl-server documentation
#########################

*******
Summary
*******
ytdl-server is a program that allows you to download videos onto a remote server
using youtube-dl. Videos are downloaded using a REST API.

See the `GitLab repo`_ for general information and installation instructions.

.. _GitLab repo: https://gitlab.com/adralioh/ytdl-server

See :doc:`api` for information about the REST API.

See :doc:`config` for configuring the program.

See :doc:`version` for information about the program's versioning scheme.

See :doc:`faq` for common questions.

*****************
Table of contents
*****************
.. toctree::
   :maxdepth: 3
   :caption: Contents:

   api
   config
   custom_opts
   version
   faq
