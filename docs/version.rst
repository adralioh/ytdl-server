#################
Versioning scheme
#################

ytdl-server uses three separate version numbers in order to indicate what
aspects of the program have changed:

:MAIN: The main version number tracks all changes made to the program, including
       when the other versions change.
:API:  The API version number tracks changes to the :doc:`api`.
:SCHEMA: The schema version number keeps track of the database's SQL schema.
         Currently, there have been no schema changes.

The MAIN and API versions use `semantic versioning`_, whereas the SCHEMA version
only changes when there is a breaking change.

.. _semantic versioning: https://semver.org/

See the `changelog`_ for details about each version.

.. _changelog: https://gitlab.com/adralioh/ytdl-server/-/blob/master/CHANGELOG.md

The following table shows how the API and SCHEMA version have changed in
relation to the MAIN version:

.. csv-table:: Versions
   :header: MAIN, API, SCHEMA

   1.1.8, 1.1.1, 1
   1.1.7, 1.1.1, 1
   1.1.6, 1.1.0, 1
   1.1.5, 1.1.0, 1
   1.1.4, 1.1.0, 1
   1.1.3, 1.1.0, 1
   1.1.2, 1.1.0, 1
   1.1.1, 1.1.0, 1
   1.1.0, 1.1.0, 1
   1.0.2, 1.0.0, 1
   1.0.1, 1.0.0, 1
   1.0.0, 1.0.0, 1
