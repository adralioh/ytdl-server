# Testing and development
First, you must install the development dependencies:
```bash
pip install -r ../requirements-dev.txt
```

## Linting
This project uses Flake8 and Mypy for linting and type-checking. Both programs
must report no errors for code to be accepted.

> Both commands should be run from the project's root dir.

Run Mypy:
```bash
mypy ytdl_server tests
```

Run Flake8:
```bash
flake8 ytdl_server tests
```

## Unit tests
Unit tests are used to test the Flask API and some utility functions.

Run the unit tests:
```bash
python -m unittest discover -t .. unit
```

## Functional tests
Functional tests are blackbox tests that are used to test running jobs.

These tests require a working ytdl-server stack. A docker-compose file is
provided to set it up.
```bash
docker-compose -p ytdl-test up
```

The `../ytdl_server/` source dir is mounted within the containers so that you
can tests changes without rebuilding the images. Simply restart the containers.

The `videos/` dir is also mounted so that the tests can check the downloaded
files. If you get a permission-denied error when running tests, check
`docker-compose.override.yml` and change the `UID` env-var to match your UID.
You'll need to rebuild the images for it to take effect. Also make sure that you
own the `videos/` dir on the host.

Once the docker-compose stack is running, you can run the functional tests:
```bash
python -m unittest discover -t .. functional
```

## Documentation
The documentation is built using Sphinx:
```bash
cd docs/
make html
```

The generated docs will be written to `docs/_build/html/`

## PyPI package
The [PyPI package](https://pypi.org/project/ytdl-server/) is built using
[build](https://pypa-build.readthedocs.io/en/latest/) and uploaded to PyPI using
Twine:
```bash
python -m build
python -m twine upload dist/*
```
