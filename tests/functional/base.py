from __future__ import annotations

import hashlib
import os
import time
import unittest
import unittest.mock as mock
import uuid
from dataclasses import dataclass, field
from datetime import datetime
from pathlib import Path
from typing import TYPE_CHECKING
from urllib.parse import urljoin

import requests

if TYPE_CHECKING:
    from collections.abc import Container, Mapping, Sequence
    from typing import Any, Final, Optional, Union

_SCRIPT_DIR: Final = os.path.dirname(os.path.realpath(__file__))


@dataclass(frozen=True)
class Video:
    """Dataclass for videos that are downloaded by tests"""

    name: str
    """Base filename of the video"""
    path: Path = field(init=False)
    """Path object pointing to the local path that the video will be
    downloaded to"""
    url: str = field(init=False)
    """URL that the video will be downloaded from"""
    checksum: str
    """SHA1 checksum of the file as a hex string"""

    base_path: Union[str, os.PathLike[str]] = field(repr=False)
    """Local directory that the video will be downloaded to

    This is used to set `path`, which can't be set directly
    """
    base_url: str = field(repr=False)
    """Base URL that the video will be downloaded from

    This is used to set `url`, which can't be set directly
    """

    def __post_init__(self) -> None:
        object.__setattr__(self, 'path', Path(self.base_path, self.name))
        object.__setattr__(self, 'url', urljoin(self.base_url, self.name))


class TestBase(unittest.TestCase):
    maxDiff = 80*16

    API_URL: Final = (
        os.environ.get('YTDL_TEST_API_URL') or 'http://localhost:8000'
    )
    """Base URL used to connect to the REST API of ytdl-server

    This is set to the env var 'YTDL_TEST_API_URL' if it exists
    """

    VIDEO_BASE_URL: Final = (
        os.environ.get('YTDL_TEST_VIDEO_URL') or 'http://videos'
    )
    """Base URL of the host to download videos from

    This is set to the env var 'YTDL_TEST_VIDEO_URL' if it exists
    """
    VIDEO_DIR: Final = Path(
        os.environ.get('YTDL_TEST_VIDEO_DIR') or
        os.path.join(_SCRIPT_DIR, '../videos')
    ).resolve()
    """Local dir that youtube-dl will download videos to

    This should be bind-mounted to the '/videos' dir within the worker
    container

    This is set to the env var 'YTDL_TEST_VIDEO_DIR' if it exists

    The default value is the path that the test Docker-compose file uses
    """

    VIDEOS: Final = (
        Video(
            name='1.ogg', checksum='c5508a479a30e5547e36df967780080141d0c968',
            base_path=VIDEO_DIR, base_url=VIDEO_BASE_URL
        ),
        Video(
            name='2.ogg', checksum='37c139190f1303b76fdc10d795792b9a1d8285e3',
            base_path=VIDEO_DIR, base_url=VIDEO_BASE_URL,
        ),
        Video(
            name='3.ogg', checksum='16957955bb4d64cba334da33cae68c41a7222aef',
            base_path=VIDEO_DIR, base_url=VIDEO_BASE_URL,
        )
    )
    """List of videos that will be downloaded"""

    _created_jobs: list[str]
    """List of job IDs that have been created by the current test

    When each test finishes, `tearDown()` waits for all the jobs to
    finish so that it can delete any video files that were downloaded
    before moving on to the next test
    """

    # TODO: might be possible to move this to setUpModule()
    @classmethod
    def setUpClass(cls) -> None:
        """Verify that the ytdl-server REST API is accessible, that
        the local video dir exists, and that the local video dir is
        empty

        If either condition is False, this function will raise an
        exception, which causes all the tests within this class to not
        be run
        """
        try:
            requests.get(cls.API_URL)
        except requests.exceptions.RequestException as exception:
            raise RuntimeError(
                'Unable to connect to ytdl-server. Is it running?\n'
                'Make sure that the YTDL_TEST_API_URL env var is set'
                ' correctly.\n'
                f'Current value: {cls.API_URL}'
            ) from exception

        if not cls.VIDEO_DIR.exists():
            raise RuntimeError(
                f"Video dir doesn't exist: {cls.VIDEO_DIR}\n"
                "The dir must be mounted to '/videos' within the worker"
                ' container\n'
                'You can change the path via the YTDL_TEST_VIDEO_DIR env var'
            )

        if not cls._is_empty(cls.VIDEO_DIR):
            raise RuntimeError(f'Video dir must be empty: {cls.VIDEO_DIR}')

    @staticmethod
    def _is_empty(dir_: Path) -> bool:
        """Return 'True' if the given dir is empty"""
        return not any(dir_.iterdir())

    def setUp(self) -> None:
        self._created_jobs = []

    def tearDown(self) -> None:
        """Delete all videos, if any, that were downloaded by the
        test"""
        # Wait for running jobs to finish to ensure that no files are
        # created after the teardown
        for job_id in self._created_jobs:
            try:
                self.get_completed(job_id, cancel=True)
            except requests.exceptions.HTTPError:
                # Ignore errors raised by `raise_for_status()` since we
                # don't care about the return code when cleaning up
                pass

        for file in self.VIDEO_DIR.iterdir():
            if file.is_file():
                file.unlink()
            else:
                raise NotImplementedError(
                    f'Video dir contains a directory: {file}'
                )

    def post_job(
        self, urls: Optional[Sequence[str]] = None,
        ytdl_opts: Optional[Mapping[str, Any]] = None,
        custom_opts: Optional[Mapping[str, Any]] = None
    ) -> requests.Response:
        """Create a new job with the given arguments, and return the
        POST response

        The function also keeps track of all jobs that were created so
        that any videos they download to the video dir can be deleted
        when the test ends
        """
        url = urljoin(self.API_URL, '/jobs/')
        data: dict[str, Any] = {}
        if urls is not None:
            data['urls'] = urls
        if ytdl_opts is not None:
            data['ytdl_opts'] = ytdl_opts
        if custom_opts is not None:
            data['custom_opts'] = custom_opts

        response = requests.post(url, json=data)

        # Attempt to get the job ID and add it to `_created_jobs`.
        # Errors are ignored because some of the tests intentionally
        # cause the job creation to fail
        try:
            job_id = response.json()['id']
        except Exception:
            pass
        else:
            self._created_jobs.append(job_id)

        return response

    def cancel_job(self, job_id: Union[str, uuid.UUID]) -> requests.Response:
        """Attempt to cancel the given job, and return the response"""
        url = urljoin(self.API_URL, f'/jobs/{job_id}')
        data = {
            'status': 'cancelled'
        }

        response = requests.patch(url, json=data)
        return response

    def get_completed(
        self, job_id: str, *, cancel: bool = False,
        incomplete_statuses: Container[str] = frozenset((
            'queued', 'downloading'
        ))
    ) -> requests.Response:
        """Wait for the job to finish, then return the GET response

        The test will fail if the job doesn't finish within 5 seconds

        If `cancel` is `True`, the job will be cancelled if it's running

        `incomplete_statuses` is the set of job statues that are
        considered to be 'in-progress'. The job will be considered
        finished if the current status isn't contained in this set
        """
        url = urljoin(self.API_URL, f'/jobs/{job_id}')
        was_cancelled = False

        for _ in range(20):
            response = requests.get(url)
            response.raise_for_status()

            status = response.json()['status']
            if status not in incomplete_statuses:
                return response

            if cancel and not was_cancelled:
                self.cancel_job(job_id)
                was_cancelled = True

            time.sleep(0.25)

        self.fail(
            'Timed out while waiting for job to finish. '
            f'Last response: {response.text}'
        )

    def assertChecksumMatches(self, video: Video) -> None:
        """Assert that the given video file exists, and has the expected
        checksum

        This ensures that the file was downloaded successfully
        """
        self.assertTrue(
            video.path.is_file(), msg=f"File doesn't exist: {video.name}"
        )

        with video.path.open('rb') as file:
            actual_hash = hashlib.sha1(file.read())
        actual_bytes = actual_hash.digest()
        actual_hex = actual_hash.hexdigest()

        expected_bytes = bytes.fromhex(video.checksum)

        self.assertEqual(
            expected_bytes, actual_bytes, msg=(
                f"File '{video.name}' has an invalid SHA1 checksum\n"
                f'Expected: {video.checksum}\n'
                f'Actual: {actual_hex}'
            )
        )

    def assertJobSucceeded(self, get_response: requests.Response) -> None:
        """Asserts that the job did not fail based on the given GET
        response"""
        status = get_response.json()['status']
        self.assertEqual(
            status, 'finished',
            msg=(
                'Job did not complete successfully. Status:\n'
                f'{get_response.text}'
            )
        )

    def assertIsTimestamp(self, timestamp: str) -> None:
        """Assert that the given string is a valid ISO timestamp"""
        self.assertIsInstance(timestamp, str, msg=(
            f'timestamp should be a string. '
            f'actual type: {type(timestamp).__name__}. value: {timestamp}'
        ))

        try:
            datetime.fromisoformat(timestamp)
        except ValueError as e:
            raise AssertionError(
                f'not a valid ISO timestamp: {timestamp}'
            ) from e

    def assertIsLog(
        self, log: Mapping[Any, Any], last_timestamp: Optional[str] = None
    ) -> None:
        """Verify that the given log entry from a job GET response is
        valid

        If `last_timestamp` is given, the timestamp of the log entry
        will be checked to make sure it's newer than `last_timestamp`.
        This is used to ensure that the logs are in chronological order
        """
        self.assertEqual(log, {
            'level': mock.ANY,
            'message': mock.ANY,
            'timestamp': mock.ANY
        })
        self.assertIn(log['level'], {'debug', 'warning', 'error'})
        self.assertIsInstance(log['message'], str)
        self.assertIsTimestamp(log['timestamp'])

        if last_timestamp:
            old_timestamp = datetime.fromisoformat(last_timestamp)
            new_timestamp = datetime.fromisoformat(log['timestamp'])
            self.assertTrue(
                new_timestamp >= old_timestamp,
                msg=(
                    "Logs aren't in chronological order : "
                    f'{log["timestamp"]!r} < {last_timestamp!r}'
                )
            )

    def _test_custom_opts(
        self, custom_opts: dict[str, Any],
        expected_ytdl_opts: dict[str, Any],
        ytdl_opts: Optional[dict[str, Any]] = None
    ) -> dict[str, Any]:
        """Creates a job with the given custom_opts, and verifies that
        the merged ytdl_opts match what is expected

        The job is always created with the 'simulate' ytdl_opt.

        Returns the get response data once the job is completed.
        """
        if ytdl_opts is None:
            ytdl_opts = {}

        post_response = self.post_job(
            urls=(self.VIDEOS[0].url,),
            # Add 'simulate' to the ytdl_opts so nothing is downloaded
            ytdl_opts=ytdl_opts | {'simulate': True},
            custom_opts=custom_opts
        )
        self.assertEqual(202, post_response.status_code)
        job_id = post_response.json()['id']

        # Wait for the job to finish
        get_response = self.get_completed(job_id)
        self.assertJobSucceeded(get_response)
        data: dict[str, Any] = get_response.json()
        self.assertIsInstance(data, dict)

        self.assertEqual(
            data['ytdl_opts'],
            # Add 'simulate' to the ytdl_opts since it's always expected
            expected_ytdl_opts | {'simulate': True}
        )

        return data
