from __future__ import annotations

import unittest.mock as mock
import uuid

from ..util import assertValidStatusURL
from .base import TestBase


class TestCancel(TestBase):
    def test_cancel(self) -> None:
        """Cancel a job"""
        post_response = self.post_job(
            urls=(self.VIDEOS[0].url,),
            ytdl_opts={
                # Throttle the download speed so that the job never
                # finishes
                'ratelimit': 1
            }
        )
        post_response.raise_for_status()
        job_id = post_response.json()['id']

        cancel_response = self.cancel_job(job_id)
        self.assertEqual(cancel_response.status_code, 202)

        with self.subTest(msg='Check response metadata'):
            self.assertEqual(
                cancel_response.headers['Content-Type'], 'application/json'
            )

        data = cancel_response.json()
        expected_json = {
            'id': job_id,
            'status_url': mock.ANY,
            'description': mock.ANY
        }
        self.assertEqual(expected_json, data)

        with self.subTest(msg='Check data types'):
            assertValidStatusURL(job_id, data['status_url'])
            self.assertIsInstance(data['description'], str)

        # Wait for the job to be cancelled
        get_response = self.get_completed(job_id)

        with self.subTest(msg='Verify the status changes'):
            status = get_response.json()['status']
            self.assertEqual('cancelled', status)

        with self.subTest(msg='Attempt to cancel an already-cancelled job'):
            new_cancel_response = self.cancel_job(job_id)
            # Status code is 200 instead of 202 since the job was
            # already cancelled
            self.assertEqual(new_cancel_response.status_code, 200)

    def test_404(self) -> None:
        """Attempt to cancel a job that doesn't exist"""
        random_uuid = uuid.uuid4()

        response = self.cancel_job(random_uuid)
        self.assertEqual(response.status_code, 404)

    def test_already_finished(self) -> None:
        """Attempt to cancel a job that already finished"""
        post_response = self.post_job(
            urls=(self.VIDEOS[0].url,),
        )
        post_response.raise_for_status()
        job_id = post_response.json()['id']

        # Wait for the job to finish
        self.get_completed(job_id)

        cancel_response = self.cancel_job(job_id)
        self.assertEqual(cancel_response.status_code, 400)
