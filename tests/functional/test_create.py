from __future__ import annotations

from pathlib import Path
import unittest.mock as mock

from ..util import assertIsUUID, assertValidStatusURL
from .base import TestBase


class TestCreate(TestBase):
    def test_check_return_data(self) -> None:
        """Check the return data when creating a job"""
        expected_json = {
            'id': mock.ANY,
            'status_url': mock.ANY
        }
        response = self.post_job(
            urls=(self.VIDEOS[0].url,),
            ytdl_opts={
                'simulate': True
            }
        )

        self.assertEqual(response.status_code, 202)
        self.assertEqual(response.headers['Content-Type'], 'application/json')

        data = response.json()
        self.assertEqual(data, expected_json)
        assertIsUUID(data['id'])
        assertValidStatusURL(data['id'], data['status_url'])

    def test_download(self) -> None:
        """Create a job, and verify it downloads"""
        response = self.post_job(
            urls=tuple(video.url for video in self.VIDEOS),
            ytdl_opts={
                'outtmpl': '%(id)s.%(ext)s'
            }
        )
        self.assertEqual(response.status_code, 202)
        job_id = response.json()['id']

        # Wait for the job to finish
        get_response = self.get_completed(job_id)
        self.assertJobSucceeded(get_response)

        for video in self.VIDEOS:
            self.assertChecksumMatches(video)

    def test_ytdl_opt(self) -> None:
        """Create a job, and verify that 'ytdl_opts' works"""
        post_response = self.post_job(
            urls=(self.VIDEOS[0].url,),
            ytdl_opts={
                'outtmpl': 'CUSTOM-PATH'
            }
        )
        self.assertEqual(post_response.status_code, 202)
        job_id = post_response.json()['id']

        # Wait for the job to finish
        get_response = self.get_completed(job_id)
        self.assertJobSucceeded(get_response)
        data = get_response.json()

        self.assertIn('outtmpl', data['ytdl_opts'])
        self.assertEqual(data['ytdl_opts']['outtmpl'], 'CUSTOM-PATH')

        output_path = Path(self.VIDEO_DIR, 'CUSTOM-PATH')
        self.assertTrue(output_path.is_file())

    def test_custom_opts(self) -> None:
        """Create a job with custom_opts"""
        self._test_custom_opts(
            custom_opts={
                'convertsubtitles': 'ass',
                'datebefore': '20210101',
                'extractaudio': {
                    'audioformat': 'opus'
                }
            },
            expected_ytdl_opts={
                # Default ytdl_opt
                'cachedir': mock.ANY,

                'format': 'bestaudio/best',
                'final_ext': 'opus',
                'daterange': 'DateRange: 0001-01-01 - 2021-01-01',
                'postprocessors': [
                    {
                        'key': 'FFmpegSubtitlesConvertor',
                        'format': 'ass'
                    },
                    {
                        'key': 'FFmpegExtractAudio',
                        'preferredcodec': 'opus',
                        'preferredquality': '5',
                        'nopostoverwrites': False
                    }
                ]
            }
        )

    def test_merge_postprocessors(self) -> None:
        """Create a job, and verify that the ytdl_opts postprocessors
        are merged with the custom_opts postprocessors

        The ytdl_opts postprocessors should be appended to the
        custom_opts postprocessors
        """
        self._test_custom_opts(
            custom_opts={
                'convertsubtitles': 'ass',
                'extractaudio': {
                    'audioformat': 'opus'
                }
            },
            ytdl_opts={
                'postprocessors': [
                    {
                        'key': 'XAttrMetadata'
                    },
                    {
                        'key': 'EmbedThumbnail',
                        'already_have_thumbnail': False
                    }
                ]
            },
            expected_ytdl_opts={
                # Default ytdl_opt
                'cachedir': mock.ANY,

                'format': 'bestaudio/best',
                'final_ext': 'opus',
                'postprocessors': [
                    {
                        'key': 'FFmpegSubtitlesConvertor',
                        'format': 'ass'
                    },
                    {
                        'key': 'FFmpegExtractAudio',
                        'preferredcodec': 'opus',
                        'preferredquality': mock.ANY,
                        'nopostoverwrites': mock.ANY
                    },
                    {
                        'key': 'XAttrMetadata'
                    },
                    {
                        'key': 'EmbedThumbnail',
                        'already_have_thumbnail': False
                    }
                ]
            }
        )

    def test_sensitive_opts(self) -> None:
        """Create a job, and verify that sensitive opts are censored"""
        post_response = self.post_job(
            urls=(self.VIDEOS[0].url,),
            ytdl_opts={
                'simulate': True,
                'password': 'SENSITIVE'
            }
        )
        self.assertEqual(post_response.status_code, 202)
        job_id = post_response.json()['id']

        # Wait for the job to finish
        get_response = self.get_completed(job_id)
        self.assertJobSucceeded(get_response)
        data = get_response.json()

        self.assertIn('password', data['ytdl_opts'])
        self.assertIsNone(data['ytdl_opts']['password'])

    def test_invalid_outtmpl(self) -> None:
        """Attempt to create jobs with invalid outtmpl values"""
        for outtmpl in ('/not-in-video-dir', '../escape-with-dots'):
            with self.subTest(outtmpl=outtmpl):
                response = self.post_job(
                    urls=(self.VIDEOS[0].url,),
                    ytdl_opts={
                        'outtmpl': outtmpl
                    }
                )
                self.assertEqual(response.status_code, 403)

    def test_disallow_path_seps(self) -> None:
        """Attempt to create jobs with options that contain illegal path
        seps"""
        for ytdl_opt in ('outtmpl_na_placeholder', 'merge_output_format'):
            with self.subTest(ytdl_opt=ytdl_opt):
                response = self.post_job(
                    urls=(self.VIDEOS[0].url,),
                    ytdl_opts={
                        ytdl_opt: 'has/pathsep'
                    }
                )
                self.assertEqual(response.status_code, 403)

    def test_allow_no_path_seps(self) -> None:
        """Create jobs with options that don't contain illegal path
        seps"""
        response = self.post_job(
            urls=(self.VIDEOS[0].url,),
            ytdl_opts={
                'outtmpl_na_placeholder': 'no-pathsep',
                'merge_output_format': 'no-pathsep',
                'simulate': True
            }
        )
        self.assertEqual(response.status_code, 202)
