from __future__ import annotations

import unittest.mock as mock
import uuid
from urllib.parse import urljoin

import requests

from ..util import assertIsUUID
from .base import TestBase


# TODO: add test for checking the status of a failed job
class TestGet(TestBase):
    def test_completed(self) -> None:
        """Check the status of a completed job"""
        videos = [video.url for video in self.VIDEOS]
        expected_json = {
            'id': mock.ANY,
            'status': 'finished',
            'urls': videos,
            'ytdl_opts': mock.ANY,
            'started': mock.ANY,
            'finished': mock.ANY,
            'error': None,
            'logs': mock.ANY,
            'progress': mock.ANY
        }

        post_response = self.post_job(
            urls=videos
        )
        post_response.raise_for_status()
        job_id = post_response.json()['id']

        # Wait for the job to finish
        get_response = self.get_completed(job_id)
        self.assertEqual(get_response.status_code, 200)

        with self.subTest(msg='Check response metadata'):
            self.assertEqual(
                get_response.headers['Content-Type'], 'application/json'
            )

        data = get_response.json()
        self.assertEqual(expected_json, data)

        with self.subTest(msg='Check data types'):
            assertIsUUID(data['id'])
            self.assertIsInstance(data['ytdl_opts'], dict)
            self.assertIsInstance(data['logs'], list)
            self.assertIsInstance(data['progress'], list)
            self.assertIsTimestamp(data['started'])
            self.assertIsTimestamp(data['finished'])

        with self.subTest(msg='Check logs'):
            last_timestamp = None
            self.assertTrue(len(data['logs']) > 0)
            for log in data['logs']:
                self.assertIsLog(log, last_timestamp)
                last_timestamp = log['timestamp']

        with self.subTest(msg='Check progress'):
            self.assertEqual(len(videos), len(data['progress']))
            for progress in data['progress']:
                self.assertIsInstance(progress, dict)

    def test_404(self) -> None:
        """Attempt to get a job that doesn't exist"""
        random_uuid = uuid.uuid4()
        url = urljoin(self.API_URL, f'/jobs/{random_uuid}')

        response = requests.get(url)
        self.assertEqual(response.status_code, 404)

    def test_in_progress(self) -> None:
        """Get the status of a job that hasn't finished"""
        post_response = self.post_job(
            urls=(self.VIDEOS[0].url,),
            ytdl_opts={
                # Throttle the download speed so that the job never
                # finishes
                'ratelimit': 1
            }
        )
        post_response.raise_for_status()
        job_id = post_response.json()['id']

        url = urljoin(self.API_URL, f'/jobs/{job_id}')
        get_response = requests.get(url)
        get_response.raise_for_status()
        self.assertEqual(get_response.status_code, 200)

        status = get_response.json()['status']
        self.assertIn(status, {'queued', 'downloading'})

        # Wait for the job to start downloading if it's still queued
        #
        # Since jobs are asynchronous, it might've already started
        # downloading before the above GET request, so we can't
        # reliably test the 'queued' status
        with self.subTest(msg='Check downloading status'):
            if status == 'queued':
                get_response = self.get_completed(
                    job_id, incomplete_statuses=frozenset(('queued',))
                )
                status = get_response.json()['status']
                self.assertEqual('downloading', status)
            else:
                self.skipTest(
                    'Job was started by a worker too quickly. '
                    'Unable to test for the "queued" status'
                )
