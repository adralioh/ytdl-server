from __future__ import annotations

import contextlib
import logging
import os
import tempfile
import unittest
import unittest.mock as mock
import uuid
from typing import cast, TYPE_CHECKING

import ytdl_server.flask as flask
import ytdl_server.job as job
from ytdl_server.error import http_error

if TYPE_CHECKING:
    from collections.abc import Generator
    from typing import Any, Optional

    from werkzeug.test import TestResponse

    from ytdl_server.util import Job, Log, Progress, StatusEnum


class TestBase(unittest.TestCase):
    """Base class used as the parent for the other test job classes"""

    maxDiff = 80*16

    _default_config = {
        'database_uri': 'NOT_USED'
    }
    """Default test_config that is given to the Flask app

    This is used by the config module instead of loading a config file

    This will be merged with `extra_config` in `setUpApp()`
    """

    def setUp(self) -> None:
        self.random_uuid = uuid.uuid4()

        # Mock `flask._init_db`
        self._patcher_db = mock.patch('ytdl_server.flask._init_db')
        self.mock_db = self._patcher_db.start()
        self.addCleanup(self._patcher_db.stop)

        # Mock `db_util.get_job()`
        self._patcher_db_get_job = mock.patch('ytdl_server.db_util.get_job')
        self.mock_db_get_job = self._patcher_db_get_job.start()
        self.addCleanup(self._patcher_db_get_job.stop)

        # Mock `db_util.get_job_status()`
        self._patcher_db_get_job_status = mock.patch(
            'ytdl_server.db_util.get_job_status'
        )
        self.mock_db_get_job_status = self._patcher_db_get_job_status.start()
        self.addCleanup(self._patcher_db_get_job_status.stop)

        # Mock `db_util.get_logs()`
        self._patcher_db_get_logs = mock.patch('ytdl_server.db_util.get_logs')
        self.mock_db_get_logs = self._patcher_db_get_logs.start()
        self.addCleanup(self._patcher_db_get_logs.stop)

        # Mock `db_util.get_progress()`
        self._patcher_db_get_progress = mock.patch(
            'ytdl_server.db_util.get_progress'
        )
        self.mock_db_get_progress = self._patcher_db_get_progress.start()
        self.addCleanup(self._patcher_db_get_progress.stop)

        # Mock `db_util.insert_job()`
        self._patcher_db_insert_job = mock.patch(
            'ytdl_server.db_util.insert_job'
        )
        self.mock_db_insert_job = self._patcher_db_insert_job.start()
        self.addCleanup(self._patcher_db_insert_job.stop)
        # Set the return value to the predetermined job ID
        self.mock_db_insert_job.return_value = self.random_uuid

        # Mock `task.download()`
        self._patcher_dl = mock.patch('ytdl_server.task.download')
        self.mock_dl = self._patcher_dl.start()
        self.addCleanup(self._patcher_dl.stop)

    def setUpApp(self, extra_config: Optional[dict[str, Any]] = None) -> None:
        """Create the Flask app

        `extra_config` will be merged into the app's config if given,
        overwriting any existing options
        """
        if extra_config is None:
            extra_config = {}
        config = self._default_config | extra_config

        self.app = flask.create_app(config)
        self.client = self.app.test_client()

    def setUpTempDir(self) -> None:
        """Create an empty temp dir

        The name of the dir will be assigned to `self.temp_dir`
        """
        self._temp = tempfile.TemporaryDirectory(suffix='.ytdl_server-tests')
        self.temp_dir = self._temp.name
        self.addCleanup(self._temp.cleanup)

    def setUpParentDir(self) -> None:
        """Create a temp dir that's guaranteed to have a parent dir,
        and change to it

        Allows using '..' without the test failing if the test is run
        while the current dir is set to '/'

        Creates the following file structure:
            TEMP_DIR.ytdl_server-tests/
            └── child/
        """
        self.setUpTempDir()

        current_dir = os.getcwd()
        child_dir = os.path.join(self.temp_dir, 'child')

        os.mkdir(child_dir)
        os.chdir(child_dir)
        self.addCleanup(os.chdir, current_dir)

    def patch_job(self, job: Job) -> None:
        """Patch the Job object returned by `get_job()`"""
        self.mock_db_get_job.return_value = job

    def patch_job_status(self, status: StatusEnum) -> None:
        """Patch the status returned by `get_job_status()`"""
        self.mock_db_get_job_status.return_value = status

    def patch_log(self, logs: tuple[Log, ...]) -> None:
        """Patch the Log tuple returned by `get_log()`"""
        self.mock_db_get_logs.return_value = logs

    def patch_progress(self, progress: tuple[Progress, ...]) -> None:
        """Patch the Progress tuple returned by `get_progress()`"""
        self.mock_db_get_progress.return_value = progress

    def patch_celery_state(self, state: str) -> None:
        """Patch the state returned by
        `task.download.AsyncResult().state`"""
        self.mock_dl.AsyncResult.return_value.state = state

    def _test_create_succeed(
        self, *, status_code: int = 202,
        app_config: Optional[dict[str, Any]] = None,
        request_ytdl_opts: Optional[dict[str, Any]] = None,
        request_custom_opts: Optional[dict[str, Any]] = None,
        response_ytdl_opts: Optional[dict[str, Any]] = None,
        response_custom_opts: Optional[dict[str, Any]] = None
    ) -> None:
        """Creating a job with the given options and assert that it
        succeeds

        status_code: Expected HTTP status code.
        app_config: Flask app config.
        request_ytdl_opts: ytdl_opts used to create the job.
        request_custom_opts: custom_opts used to create the job.
        response_ytdl_opts: Expected ytdl_opts that are passed to the
            task. Defaults to the same value as `request_ytdl_opts` if
            not `None`; otherwise defaults to an empty dict.
        response_custom_opts: Expected custom_opts that are passed to
            the task. Defaults to the same value as
            `request_custom_opts` if not `None`; otherwise defaults to
            an empty dict.
        """
        if response_ytdl_opts is None:
            response_ytdl_opts = request_ytdl_opts or {}
        if response_custom_opts is None:
            response_custom_opts = request_custom_opts or {}

        self.setUpApp(app_config)
        request_data = {
            'urls': ('URL1', 'URL2', 'URL3'),
            'ytdl_opts': request_ytdl_opts,
            'custom_opts': request_custom_opts
        }

        response = self.client.post('/jobs/', json=request_data)
        self.assertEqual(
            status_code, response.status_code, msg=response.get_json()
        )
        self.assertYTDLOpts(
            ytdl_opts=response_ytdl_opts, custom_opts=response_custom_opts
        )

    def _test_create_fail(
        self, *, status_code: int = 400,
        app_config: Optional[dict[str, Any]] = None,
        request_ytdl_opts: Optional[dict[str, Any]] = None,
        request_custom_opts: Optional[dict[str, Any]] = None
    ) -> None:
        """Creating a job with the given options and assert that it
        fails

        status_code: Expected HTTP status code.
        app_config: Flask app config.
        request_ytdl_opts: ytdl_opts used to create the job.
        request_custom_opts: custom_opts used to create the job.

        Returns the POST response.
        """
        self.setUpApp(app_config)
        request_data = {
            'urls': ('URL1', 'URL2', 'URL3'),
            'ytdl_opts': request_ytdl_opts,
            'custom_opts': request_custom_opts
        }

        response = self.client.post('/jobs/', json=request_data)
        self.assertEqual(
            status_code, response.status_code, msg=response.get_json()
        )
        self.assertTaskNotCalled()

    def assertYTDLOpts(
        self, ytdl_opts: dict[str, Any], custom_opts: dict[str, Any]
    ) -> None:
        """Assert that the correct ytdl opts were passed to
        `task.download.apply_async()`

        The value of other args passed to the function are ignored
        """
        self.mock_dl.apply_async.assert_called_once_with(
            task_id=mock.ANY, kwargs={
                'ytdl_opts': ytdl_opts, 'custom_opts': custom_opts,
                'urls': mock.ANY, 'download_dir': mock.ANY,
                'sensitive_opts': mock.ANY,
                'ytdl_module': mock.ANY, 'ytdl_class': mock.ANY,
                'daterange_module': mock.ANY, 'daterange_class': mock.ANY,
                'metadata_module': mock.ANY, 'metadata_class': mock.ANY
            }
        )

    def assertAbortNotCalled(self) -> None:
        """Assert that `task.download.AsyncResult().abort()` wasn't
        called

        Used by the `cancel_job()` tests
        """
        self.mock_dl.AsyncResult.return_value.abort.assert_not_called()

    def assertTaskNotCalled(self) -> None:
        """Assert that `task.download.apply_async()` wasn't
        called

        Used by the `create_job()` tests
        """
        self.mock_dl.apply_async.assert_not_called()

    @contextlib.contextmanager
    def assertDoesNotRaise(
        self, exception: type[BaseException], *, msg: Optional[str] = None
    ) -> Generator[None, None, None]:
        """Context manager that asserts the suite does not raise an
        exception

        This does the opposite of `assertRaises()`

        Normally when an exception is raised in a test, the test is
        reported as an error. Using this function will cause it to be
        reported as failed, which makes it more obvious that the
        exception is what we're testing for
        """
        try:
            yield None
        except exception as e:
            if msg is None:
                msg = f'{exception.__name__} was raised'
            raise AssertionError(msg) from e

    def assertIsHTTPErrorType(
        self, response: TestResponse, error: type[http_error.HTTPError]
    ) -> None:
        """Assert that the error response matches the expected HTTP
        error type"""
        response_json = response.get_json()

        self.assertIsInstance(
            response_json, dict,
            f'response is not a JSON object: {response_json!r}'
        )
        # Cast the type because we just checked it with assertIsInstance
        response_json = cast('dict[str, Any]', response_json)

        actual_error_name = response_json.get('error')
        expected_error_name = error.__name__

        self.assertEqual(
            actual_error_name, expected_error_name,
            (
                'response is the wrong HTTP error type. '
                f'Actual response: {response_json!r}'
            )
        )

    def disable_logging(self) -> None:
        """Temporarily disable all logging for the current test

        This should only be called by tests that are expected to log an
        error so that unexpected logging errors from other tests will
        still be visible
        """
        logging.disable()
        self.addCleanup(logging.disable, logging.NOTSET)


class TestJobCheckPathSeps:
    """OS-agnostic base tests for jobs._check_path_seps"""
    # Uses a subclass so that `TestJobCheckPathSeps` isn't run by
    # `unittest discover`
    class Tests(TestBase):
        def check_path_seps(self, opt_value: Any, force: bool = True) -> None:
            """Run `_check_path_seps()` with Flask context

            Also sets `force_download_dir`, which is used by the
            function
            """
            self.setUpApp({
                'force_download_dir': force
            })
            with self.app.app_context():
                job._check_path_seps('OPT_NAME', opt_value)

        def test_none(self) -> None:
            """_check_path_seps - path with no seps"""
            with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
                self.check_path_seps('name_with_no_seps')

        def test_has(self) -> None:
            """_check_path_seps - basic slash"""
            with self.assertRaises(http_error.YTDLOptionBadValue):
                self.check_path_seps('name_with/seps')

        def test_empty_string(self) -> None:
            """_check_path_seps - empty string"""
            with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
                self.check_path_seps('')

        def test_escape_char(self) -> None:
            r"""_check_path_seps - escape char

            Verifies that '\n' and '\\' aren't being confused
            """
            with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
                self.check_path_seps('name_without\nslashes')

        def test_both_slashes(self) -> None:
            """_check_path_seps - forward slash and backslash"""
            with self.assertRaises(http_error.YTDLOptionBadValue):
                self.check_path_seps('name/with/both\\slashes')

        def test_single_slash(self) -> None:
            """_check_path_seps - single slash"""
            with self.assertRaises(http_error.YTDLOptionBadValue):
                self.check_path_seps('/')

        def test_config_disabled(self) -> None:
            """_check_path_seps - has seps, but config option is disabled
            """
            with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
                self.check_path_seps('name_with/seps', False)

        def test_not_a_str(self) -> None:
            """_check_path_seps - value isn't a str"""
            with self.assertRaises(http_error.YTDLOptionBadValue):
                self.check_path_seps(1)
