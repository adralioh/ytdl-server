from __future__ import annotations

from typing import TYPE_CHECKING
import re
import unittest
import unittest.mock as mock

from ytdl_server.custom_opt import load_custom_opts
from ytdl_server.task import TaskArgs

if TYPE_CHECKING:
    from collections.abc import Mapping
    from typing import Any, Optional, Union


class TestCustomOpt(unittest.TestCase):
    maxDiff = 80*16

    def _test_custom_opts(
        self, *,
        custom_opts: Mapping[str, Any], loaded_custom_opts: dict[str, Any],
        ytdl_opts: Optional[Mapping[str, Any]] = None
    ) -> None:
        """Load the given custom_opts, and assert that the result equals
        the loaded_custom_opts

        ytdl_opts defaults to an empty dict if not given
        """
        if ytdl_opts is None:
            ytdl_opts = {}

        task_args = TaskArgs(
            urls=(),
            ytdl_opts=ytdl_opts,
            custom_opts=custom_opts,
            sensitive_opts=(), download_dir='.',
            ytdl_module='youtube_dl', ytdl_class='YoutubeDL',
            daterange_module='youtube_dl', daterange_class='DateRange',
            metadata_module='youtube_dl', metadata_class='MetadataParserPP',
            logger=mock.MagicMock()
        )

        loaded_opts = load_custom_opts(task_args)
        self.assertEqual(loaded_custom_opts, loaded_opts)

    @mock.patch('ytdl_server.custom_opt.import_object')
    def _test_daterange(
        self, dateafter: Optional[str], datebefore: Optional[str],
        mock_import: mock.MagicMock
    ) -> None:
        """Test the daterange custom_opts

        `import_object()` is mocked because it's used to import the
        `DateRange` class, which requires youtube-dl.

        Does not work if both custom_opts are `None`.
        `test_daterange_both_None()` is used to test that instead.
        """
        self._test_custom_opts(
            custom_opts={
                'dateafter': dateafter,
                'datebefore': datebefore
            },
            loaded_custom_opts={
                'daterange': mock_import.return_value.return_value
            }
        )
        mock_import.return_value.assert_called_once_with(dateafter, datebefore)

    def test_daterange(self) -> None:
        """Load the daterange custom_opts

        The dateafter and datebefore custom_opts are alternatively set
        to either a str or `None`
        """
        dateranges = (
            ('20210101', '20210201'),
            (None, '20210201'),
            ('20210101', None),
        )
        for dateafter, datebefore in dateranges:
            with self.subTest(dateafter=dateafter, datebefore=datebefore):
                self._test_daterange(dateafter, datebefore)

    @mock.patch('ytdl_server.custom_opt.import_object')
    def test_daterange_both_None(self, mock_import: mock.MagicMock) -> None:
        """Don't load the daterange custom_opts when they're both None

        The daterange custom_opt handler (_handle_daterange) shouldn't
        be called.
        """
        self._test_custom_opts(
            custom_opts={
                'dateafter': None,
                'datebefore': None
            },
            loaded_custom_opts={}
        )
        mock_import.return_value.assert_not_called()

    @mock.patch('ytdl_server.custom_opt.import_object')
    def _test_metadataparser(
        self, mock_import: mock.MagicMock, *,
        parse_metadata: Optional[list[dict[str, Any]]],
        replace_in_metadata: Optional[list[dict[str, Any]]],
        actions: tuple[Union[tuple[str, str, str], tuple[str, str, str, str]]],
    ) -> None:
        """Test the parse_metadata and replace_in_metadata custom_opts

        The `Actions` enum members are mocked with the strings
        'MOCK_INTERPRET' and 'MOCK_REPLACE'

        `import_object()` is mocked because it's used to import the
        `MetadataParserPP` class, which requires yt-dlp.

        Does not work if both custom_opts are `None`.
        `test_metadataparser_both_None()` is used to test that instead.
        """
        mock_import.return_value.Actions.INTERPRET = 'MOCK_INTERPRET'
        mock_import.return_value.Actions.REPLACE = 'MOCK_REPLACE'
        self._test_custom_opts(
            custom_opts={
                'parse_metadata': parse_metadata,
                'replace_in_metadata': replace_in_metadata
            },
            loaded_custom_opts={
                'postprocessors': [{
                        'key': 'MetadataParser',
                        'actions': actions,
                        'when': 'pre_process'
                }]
            }
        )

    def test_parse_metadata_single(self) -> None:
        """Load the parse_metadata custom_opt with a single interpreter"""
        self._test_metadataparser(
            parse_metadata=[
                {'from': 'FROM1', 'to': 'TO1'}
            ],
            replace_in_metadata=None,
            actions=(
                ('MOCK_INTERPRET', 'FROM1', 'TO1'),
            )
        )

    def test_parse_metadata_multiple(self) -> None:
        """Load the parse_metadata custom_opt with a multiple
        interpreters"""
        self._test_metadataparser(
            parse_metadata=[
                {'from': 'FROM1', 'to': 'TO1'},
                {'from': 'FROM2', 'to': 'TO2'},
                {'from': 'FROM3', 'to': 'TO3'}
            ],
            replace_in_metadata=None,
            actions=(
                ('MOCK_INTERPRET', 'FROM1', 'TO1'),
                ('MOCK_INTERPRET', 'FROM2', 'TO2'),
                ('MOCK_INTERPRET', 'FROM3', 'TO3')
            )
        )

    def test_replace_in_metadata_single(self) -> None:
        """Load the replace_in_metadata custom_opt with a single
        replacer"""
        self._test_metadataparser(
            parse_metadata=None,
            replace_in_metadata=[
                {
                    'fields': ['FIELD1'],
                    'regex': 'REGEX1', 'replace': 'REPLACE1'
                }
            ],
            actions=(
                ('MOCK_REPLACE', 'FIELD1', 'REGEX1', 'REPLACE1'),
            )
        )

    def test_replace_in_metadata_multiple_fields(self) -> None:
        """Load the replace_in_metadata custom_opt with a single
        replacer with multiple fields"""
        self._test_metadataparser(
            parse_metadata=None,
            replace_in_metadata=[
                {
                    'fields': ['FIELD1', 'FIELD2', 'FIELD3'],
                    'regex': 'REGEX1', 'replace': 'REPLACE1'
                }
            ],
            actions=(
                ('MOCK_REPLACE', 'FIELD1', 'REGEX1', 'REPLACE1'),
                ('MOCK_REPLACE', 'FIELD2', 'REGEX1', 'REPLACE1'),
                ('MOCK_REPLACE', 'FIELD3', 'REGEX1', 'REPLACE1'),
            )
        )

    def test_replace_in_metadata_multiple_replacers(self) -> None:
        """Load the replace_in_metadata custom_opt with multiple
        replacers and fields"""
        self._test_metadataparser(
            parse_metadata=None,
            replace_in_metadata=[
                {
                    'fields': ['FIELD1', 'FIELD2', 'FIELD3'],
                    'regex': 'REGEX1', 'replace': 'REPLACE1'
                },
                {
                    'fields': ['FIELD4', 'FIELD5'],
                    'regex': 'REGEX2', 'replace': 'REPLACE2'
                },
                {
                    'fields': ['FIELD6'],
                    'regex': 'REGEX3', 'replace': 'REPLACE3'
                }
            ],
            actions=(
                ('MOCK_REPLACE', 'FIELD1', 'REGEX1', 'REPLACE1'),
                ('MOCK_REPLACE', 'FIELD2', 'REGEX1', 'REPLACE1'),
                ('MOCK_REPLACE', 'FIELD3', 'REGEX1', 'REPLACE1'),

                ('MOCK_REPLACE', 'FIELD4', 'REGEX2', 'REPLACE2'),
                ('MOCK_REPLACE', 'FIELD5', 'REGEX2', 'REPLACE2'),

                ('MOCK_REPLACE', 'FIELD6', 'REGEX3', 'REPLACE3'),
            )
        )

    def test_metadataparser_both(self) -> None:
        """Load the parse_metadata and replace_in_metadata custom_opts
        simultaneously"""
        self._test_metadataparser(
            parse_metadata=[
                {'from': 'FROM1', 'to': 'TO1'},
                {'from': 'FROM2', 'to': 'TO2'}
            ],
            replace_in_metadata=[
                {
                    'fields': ['FIELD1', 'FIELD2'],
                    'regex': 'REGEX1', 'replace': 'REPLACE1'
                },
                {
                    'fields': ['FIELD3'],
                    'regex': 'REGEX2', 'replace': 'REPLACE2'
                }
            ],
            actions=(
                ('MOCK_INTERPRET', 'FROM1', 'TO1'),
                ('MOCK_INTERPRET', 'FROM2', 'TO2'),

                ('MOCK_REPLACE', 'FIELD1', 'REGEX1', 'REPLACE1'),
                ('MOCK_REPLACE', 'FIELD2', 'REGEX1', 'REPLACE1'),
                ('MOCK_REPLACE', 'FIELD3', 'REGEX2', 'REPLACE2'),
            )
        )

    @mock.patch('ytdl_server.custom_opt.import_object')
    def test_metadataparser_empty_list(
        self, mock_import: mock.MagicMock
    ) -> None:
        """Don't load the parse_metadata and replace_in_metadata
        custom_opts when they're both empty lists
        """
        self._test_custom_opts(
            custom_opts={
                'parse_metadata': [],
                'replace_in_metadata': []
            },
            loaded_custom_opts={}
        )
        mock_import.return_value.assert_not_called()

    def test_metafromtitle(self) -> None:
        """Load the metafromtitle custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'metafromtitle': '%(artist)s - %(title)s'
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'MetadataFromTitle',
                    'titleformat': '%(artist)s - %(title)s'
                }]
            }
        )

    def test_extractaudio_defaults(self) -> None:
        """Load the extractaudio custom_opt with default values"""
        self._test_custom_opts(
            custom_opts={
                'extractaudio': {}
            },
            loaded_custom_opts={
                'format': 'bestaudio/best',
                'postprocessors': [{
                    'key': 'FFmpegExtractAudio',
                    'preferredcodec': 'best',
                    'preferredquality': '5',
                    'nopostoverwrites': False
                }]
            }
        )

    def test_extractaudio_all_given(self) -> None:
        """Load the extractaudio custom_opt with all custom values"""
        self._test_custom_opts(
            custom_opts={
                'extractaudio': {
                    'audioformat': 'mp3',
                    'audioquality': 320,
                    'nopostoverwrites': True
                }
            },
            loaded_custom_opts={
                'format': 'bestaudio/best',
                'final_ext': 'mp3',
                'postprocessors': [{
                    'key': 'FFmpegExtractAudio',
                    'preferredcodec': 'mp3',
                    'preferredquality': '320',
                    'nopostoverwrites': True
                }]
            }
        )

    def test_extractaudio_existing_format(self) -> None:
        """Load the extractaudio custom_opt with an existing format

        The format shouldn't be overridden by the custom_opt
        """
        self._test_custom_opts(
            custom_opts={
                'extractaudio': {}
            },
            ytdl_opts={
                'format': 'best'
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'FFmpegExtractAudio',
                    'preferredcodec': mock.ANY,
                    'preferredquality': mock.ANY,
                    'nopostoverwrites': mock.ANY
                }]
            }
        )

    def test_extractaudio_keepvideo(self) -> None:
        """Load the extractaudio custom_opt when keepvideo is True

        The format shouldn't be set by the custom_opt
        """
        self._test_custom_opts(
            custom_opts={
                'extractaudio': {}
            },
            ytdl_opts={
                'keepvideo': True
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'FFmpegExtractAudio',
                    'preferredcodec': mock.ANY,
                    'preferredquality': mock.ANY,
                    'nopostoverwrites': mock.ANY
                }]
            }
        )

    def test_remuxvideo(self) -> None:
        """Load the remuxvideo custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'remuxvideo': 'mkv'
            },
            loaded_custom_opts={
                'final_ext': 'mkv',
                'postprocessors': [{
                    'key': 'FFmpegVideoRemuxer',
                    'preferedformat': 'mkv'
                }]
            }
        )

    def test_remuxvideo_conflict_recodevideo(self) -> None:
        """Don't load the remuxvideo custom_opt when recodevideo is
        given"""
        self._test_custom_opts(
            custom_opts={
                'remuxvideo': 'mkv',
                'recodevideo': 'mp4'
            },
            loaded_custom_opts={
                'final_ext': 'mp4',
                'postprocessors': [{
                    'key': 'FFmpegVideoConvertor',
                    'preferedformat': 'mp4'
                }]
            }
        )

    def test_recodevideo(self) -> None:
        """Load the recodevideo custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'recodevideo': 'mkv'
            },
            loaded_custom_opts={
                'final_ext': 'mkv',
                'postprocessors': [{
                    'key': 'FFmpegVideoConvertor',
                    'preferedformat': 'mkv'
                }]
            }
        )

    def test_remove_chapters(self) -> None:
        """Load the remove_chapters custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'remove_chapters': {
                    'patterns': ['PAT1', 'PAT2'],
                    'ranges': [[60, 120], [180.5, 190]],
                    'force_keyframes': True
                }
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'ModifyChapters',
                    'remove_chapters_patterns': (
                        re.compile('PAT1'), re.compile('PAT2')
                    ),
                    'remove_ranges': [[60, 120], [180.5, 190]],
                    'force_keyframes': True
                }]
            }
        )

    def test_remove_chapters_none(self) -> None:
        """Load the remove_chapters custom_opt with both required args
        set to None"""
        self._test_custom_opts(
            custom_opts={
                'remove_chapters': {
                    'patterns': None,
                    'ranges': None
                }
            },
            loaded_custom_opts={}
        )

    def test_remove_chapters_empty(self) -> None:
        """Load the remove_chapters custom_opt with empty lists"""
        self._test_custom_opts(
            custom_opts={
                'remove_chapters': {
                    'patterns': [],
                    'ranges': []
                }
            },
            loaded_custom_opts={}
        )

    def test_remove_chapters_force_keyframes_none(self) -> None:
        """Load the remove_chapters custom_opt with force_keyframes set
        to `None`"""
        self._test_custom_opts(
            custom_opts={
                'remove_chapters': {
                    'patterns': ['PAT1', 'PAT2'],
                    'force_keyframes': None
                }
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'ModifyChapters',
                    'remove_chapters_patterns': (
                        re.compile('PAT1'), re.compile('PAT2')
                    ),
                    'force_keyframes': False
                }]
            }
        )

    def test_custom_opt_addmetadata(self) -> None:
        """Load the addmetadata custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'addmetadata': True
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'FFmpegMetadata'
                }]
            }
        )

    def test_custom_opt_addmetadata_false(self) -> None:
        """Don't load the addmetadata custom_opt when the value is False"""
        self._test_custom_opts(
            custom_opts={
                'addmetadata': False
            },
            loaded_custom_opts={}
        )

    def test_custom_opt_addchapters(self) -> None:
        """Load the addchapters custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'addchapters': True
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'FFmpegMetadata',
                    'add_chapters': True,
                    'add_metadata': False
                }]
            }
        )

    def test_custom_opt_addchapters_false(self) -> None:
        """Load the addchapters custom_opt when the value is False"""
        self._test_custom_opts(
            custom_opts={
                'addchapters': False
            },
            loaded_custom_opts={}
        )

    def test_custom_opt_add_metadata_and_addchapters(self) -> None:
        """Load the addmetadata and addchapters custom_opts at the same
        time"""
        self._test_custom_opts(
            custom_opts={
                'addchapters': True,
                'addmetadata': True
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'FFmpegMetadata',
                    'add_chapters': True,
                    'add_metadata': True
                }]
            }
        )

    def test_custom_opt_convertsubtitles(self) -> None:
        """Load the convertsubtitles custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'convertsubtitles': 'ass'
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'FFmpegSubtitlesConvertor',
                    'format': 'ass'
                }]
            }
        )

    def test_custom_opt_convertthumbnails(self) -> None:
        """Load the convertthumbnails custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'convertthumbnails': 'png'
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'FFmpegThumbnailsConvertor',
                    'format': 'png',
                    'when': 'before_dl'
                }]
            }
        )

    def test_custom_opt_embedsubtitles(self) -> None:
        """Load the embedsubtitles custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'embedsubtitles': True
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'FFmpegEmbedSubtitle'
                }]
            }
        )

    def test_custom_opt_embedsubtitles_false(self) -> None:
        """Don't load the embedsubtitles custom_opt when the value is
        False"""
        self._test_custom_opts(
            custom_opts={
                'embedsubtitles': False
            },
            loaded_custom_opts={}
        )

    def test_custom_opt_embedthumbnail(self) -> None:
        """Load the embedthumbnail custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'embedthumbnail': True
            },
            loaded_custom_opts={
                'writethumbnail': True,
                'postprocessors': [{
                    'key': 'EmbedThumbnail',
                    'already_have_thumbnail': False
                }]
            }
        )

    def test_custom_opt_embedthumbnail_have_thumbnail(self) -> None:
        """Load the embedthumbnail custom_opt, and verify that
        already_have_thumbnail is True when writethumbnail is explicitly
        given"""
        self._test_custom_opts(
            custom_opts={
                'embedthumbnail': True
            },
            ytdl_opts={
                'writethumbnail': True
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'EmbedThumbnail',
                    'already_have_thumbnail': True
                }]
            }
        )

    def test_custom_opt_embedthumbnail_false(self) -> None:
        """Don't load the embedthumbnail custom_opt when the value is
        False"""
        self._test_custom_opts(
            custom_opts={
                'embedthumbnail': False
            },
            loaded_custom_opts={}
        )

    def test_custom_opt_split_chapters(self) -> None:
        """Load the split_chapters custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'split_chapters': {
                    'force_keyframes': True
                }
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'FFmpegSplitChapters',
                    'force_keyframes': True
                }]
            }
        )

    def test_custom_opt_split_chapters_defaults(self) -> None:
        """Load the split_chapters custom_opt with default args"""
        self._test_custom_opts(
            custom_opts={
                'split_chapters': {}
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'FFmpegSplitChapters',
                    'force_keyframes': False
                }]
            }
        )

    def test_custom_opt_split_chapters_none(self) -> None:
        """Load the split_chapters custom_opt with force_keyframes set
        to None"""
        self._test_custom_opts(
            custom_opts={
                'split_chapters': {
                    'force_keyframes': None
                }
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'FFmpegSplitChapters',
                    'force_keyframes': False
                }]
            }
        )

    def test_custom_opt_xattrs(self) -> None:
        """Load the xattrs custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'xattrs': True
            },
            loaded_custom_opts={
                'postprocessors': [{
                    'key': 'XAttrMetadata'
                }]
            }
        )

    def test_custom_opt_xattrs_false(self) -> None:
        """Don't load the xattrs custom_opt when the value is False"""
        self._test_custom_opts(
            custom_opts={
                'xattrs': False
            },
            loaded_custom_opts={}
        )

    def test_custom_opt_sponsorblock(self) -> None:
        """Load the sponsorblock custom_opt"""
        self._test_custom_opts(
            custom_opts={
                'sponsorblock': {
                    'mark': ['MARK', 'SHARED'],
                    'remove': ['REMOVE', 'SHARED'],
                    'api': 'API'
                }
            },
            loaded_custom_opts={
                'postprocessors': [
                    {
                        'key': 'SponsorBlock',
                        'categories': frozenset(('MARK', 'REMOVE', 'SHARED')),
                        'api': 'API',
                        'when': 'pre_process'
                    },
                    {
                        'key': 'ModifyChapters',
                        'remove_sponsor_segments': frozenset(
                            ('REMOVE', 'SHARED')
                        ),
                        'force_keyframes': False
                    },
                    {
                        'key': 'FFmpegMetadata',
                        'add_chapters': True,
                        'add_metadata': False
                    }
                ]
            }
        )

    def test_custom_opt_sponsorblock_only_mark(self) -> None:
        """Load the sponsorblock custom_opt with only the mark field"""
        self._test_custom_opts(
            custom_opts={
                'sponsorblock': {
                    'mark': ['MARK', 'SHARED']
                }
            },
            loaded_custom_opts={
                'postprocessors': [
                    {
                        'key': 'SponsorBlock',
                        'categories': frozenset(('MARK', 'SHARED')),
                        'when': 'pre_process'
                    },
                    {
                        'key': 'FFmpegMetadata',
                        'add_chapters': True,
                        'add_metadata': False
                    }
                ]
            }
        )

    def test_custom_opt_sponsorblock_only_remove(self) -> None:
        """Load the sponsorblock custom_opt with only the remove field"""
        self._test_custom_opts(
            custom_opts={
                'sponsorblock': {
                    'remove': ['REMOVE', 'SHARED']
                }
            },
            loaded_custom_opts={
                'postprocessors': [
                    {
                        'key': 'SponsorBlock',
                        'categories': frozenset(('REMOVE', 'SHARED')),
                        'when': 'pre_process'
                    },
                    {
                        'key': 'ModifyChapters',
                        'remove_sponsor_segments': frozenset(
                            ('REMOVE', 'SHARED')
                        ),
                        'force_keyframes': False
                    }
                ]
            }
        )

    def test_custom_opt_sponsorblock_mark_none(self) -> None:
        """Load the sponsorblock custom_opt mark field is None"""
        self._test_custom_opts(
            custom_opts={
                'sponsorblock': {
                    'mark': None,
                    'remove': ['REMOVE', 'SHARED']
                }
            },
            loaded_custom_opts={
                'postprocessors': [
                    {
                        'key': 'SponsorBlock',
                        'categories': frozenset(('REMOVE', 'SHARED')),
                        'when': 'pre_process'
                    },
                    {
                        'key': 'ModifyChapters',
                        'remove_sponsor_segments': frozenset(
                            ('REMOVE', 'SHARED')
                        ),
                        'force_keyframes': False
                    }
                ]
            }
        )

    def test_custom_opt_sponsorblock_remove_none(self) -> None:
        """Load the sponsorblock custom_opt remove field is None"""
        self._test_custom_opts(
            custom_opts={
                'sponsorblock': {
                    'mark': ['MARK', 'SHARED'],
                    'remove': None
                }
            },
            loaded_custom_opts={
                'postprocessors': [
                    {
                        'key': 'SponsorBlock',
                        'categories': frozenset(('MARK', 'SHARED')),
                        'when': 'pre_process'
                    },
                    {
                        'key': 'FFmpegMetadata',
                        'add_chapters': True,
                        'add_metadata': False
                    }
                ]
            }
        )

    def test_custom_opt_sponsorblock_api_none(self) -> None:
        """Load the sponsorblock custom_opt api field is None"""
        self._test_custom_opts(
            custom_opts={
                'sponsorblock': {
                    'mark': ['MARK', 'SHARED'],
                    'api': None
                }
            },
            loaded_custom_opts={
                'postprocessors': [
                    {
                        'key': 'SponsorBlock',
                        'categories': frozenset(('MARK', 'SHARED')),
                        'when': 'pre_process'
                    },
                    {
                        'key': 'FFmpegMetadata',
                        'add_chapters': True,
                        'add_metadata': False
                    }
                ]
            }
        )

    def test_custom_opt_sponsorblock_and_remove_chapters(self) -> None:
        """Load the sponsorblock and remove_chapters custom_opts at the
        same time"""
        self._test_custom_opts(
            custom_opts={
                'remove_chapters': {
                    'patterns': ['PAT1', 'PAT2'],
                    'ranges': [[60, 120], [180.5, 190]],
                },
                'sponsorblock': {
                    'remove': ['REMOVE', 'SHARED'],
                    'template': 'TEMPLATE'
                }
            },
            loaded_custom_opts={
                'postprocessors': [
                    {
                        'key': 'SponsorBlock',
                        'categories': frozenset(('REMOVE', 'SHARED')),
                        'when': 'pre_process'
                    },
                    {
                        'key': 'ModifyChapters',
                        'remove_chapters_patterns': (
                            re.compile('PAT1'), re.compile('PAT2')
                        ),
                        'remove_ranges': [[60, 120], [180.5, 190]],
                        'remove_sponsor_segments': frozenset(
                            ('REMOVE', 'SHARED')
                        ),
                        'sponsorblock_chapter_title': 'TEMPLATE',
                        'force_keyframes': False
                    }
                ]
            }
        )

    def test_custom_opt_sponsorblock_and_remove_chapters_keyframes_sponsor(
        self
    ) -> None:
        """Load the sponsorblock and remove_chapters custom_opts at the
        same time, and verify that force_keyframes in sponsorblock is
        used"""
        self._test_custom_opts(
            custom_opts={
                'remove_chapters': {
                    'patterns': ['PAT1', 'PAT2'],
                },
                'sponsorblock': {
                    'remove': ['REMOVE', 'SHARED'],
                    'force_keyframes': True
                }
            },
            loaded_custom_opts={
                'postprocessors': [
                    {
                        'key': 'SponsorBlock',
                        'categories': frozenset(('REMOVE', 'SHARED')),
                        'when': 'pre_process'
                    },
                    {
                        'key': 'ModifyChapters',
                        'remove_chapters_patterns': (
                            re.compile('PAT1'), re.compile('PAT2')
                        ),
                        'remove_sponsor_segments': frozenset(
                            ('REMOVE', 'SHARED')
                        ),
                        'force_keyframes': True
                    }
                ]
            }
        )

    def test_custom_opt_sponsorblock_and_remove_chapters_keyframes_remove(
        self
    ) -> None:
        """Load the sponsorblock and remove_chapters custom_opts at the
        same time, and verify that force_keyframes in remove_chapters
        overrides the value in sponsorblock"""
        self._test_custom_opts(
            custom_opts={
                'remove_chapters': {
                    'patterns': ['PAT1', 'PAT2'],
                    'force_keyframes': False
                },
                'sponsorblock': {
                    'remove': ['REMOVE', 'SHARED'],
                    'force_keyframes': True
                }
            },
            loaded_custom_opts={
                'postprocessors': [
                    {
                        'key': 'SponsorBlock',
                        'categories': frozenset(('REMOVE', 'SHARED')),
                        'when': 'pre_process'
                    },
                    {
                        'key': 'ModifyChapters',
                        'remove_chapters_patterns': (
                            re.compile('PAT1'), re.compile('PAT2')
                        ),
                        'remove_sponsor_segments': frozenset(
                            ('REMOVE', 'SHARED')
                        ),
                        'force_keyframes': False
                    }
                ]
            }
        )

    def test_custom_opt_sponsorblock_et_al(self) -> None:
        """Load the sponsorblock custom_opt and all of the related
        custom_opts at once"""
        self._test_custom_opts(
            custom_opts={
                'addchapters': False,
                'addmetadata': True,
                'remove_chapters': {
                    'patterns': ['PAT1', 'PAT2'],
                    'ranges': [[60, 120], [180.5, 190]],
                },
                'sponsorblock': {
                    'mark': ['MARK', 'SHARED'],
                    'remove': ['REMOVE', 'SHARED'],
                    'template': 'TEMPLATE'
                }
            },
            loaded_custom_opts={
                'postprocessors': [
                    {
                        'key': 'SponsorBlock',
                        'categories': frozenset(('MARK', 'REMOVE', 'SHARED')),
                        'when': 'pre_process'
                    },
                    {
                        'key': 'ModifyChapters',
                        'remove_chapters_patterns': (
                            re.compile('PAT1'), re.compile('PAT2')
                        ),
                        'remove_ranges': [[60, 120], [180.5, 190]],
                        'remove_sponsor_segments': frozenset(
                            ('REMOVE', 'SHARED')
                        ),
                        'sponsorblock_chapter_title': 'TEMPLATE',
                        'force_keyframes': False
                    },
                    {
                        'key': 'FFmpegMetadata',
                        'add_chapters': False,
                        'add_metadata': True
                    }
                ]
            }
        )

    def test_final_ext_recodevideo_nonalpha(self) -> None:
        """Verify that the final_ext ytdl_opt isn't added when
        recodevideo contains non-alphanumeric characters"""
        self._test_custom_opts(
            custom_opts={
                'recodevideo': 'aac>m4a/mkv'
            },
            loaded_custom_opts={
                'postprocessors': mock.ANY
            }
        )

    def test_final_ext_remuxvideo_nonalpha(self) -> None:
        """Verify that the final_ext ytdl_opt isn't added when
        remuxvideo contains non-alphanumeric characters"""
        self._test_custom_opts(
            custom_opts={
                'remuxvideo': 'aac>m4a/mkv'
            },
            loaded_custom_opts={
                'postprocessors': mock.ANY
            }
        )

    def test_final_ext_extractaudio_best(self) -> None:
        """Verify that the final_ext ytdl_opt isn't added when
        extractaudio.audioformat is 'best'"""
        self._test_custom_opts(
            custom_opts={
                'extractaudio': {
                    'audioformat': 'best'
                }
            },
            loaded_custom_opts={
                'format': mock.ANY,
                'postprocessors': mock.ANY
            }
        )

    def test_final_ext_priority_recodevideo(self) -> None:
        """Verify that the final_ext ytdl_opt is set to recodevideo
        first"""
        self._test_custom_opts(
            custom_opts={
                'recodevideo': 'RECODE',
                'remuxvideo': 'REMUX',
                'extractaudio': {
                    'audioformat': 'EXTRACT'
                }
            },
            loaded_custom_opts={
                'final_ext': 'RECODE',
                'format': mock.ANY,
                'postprocessors': mock.ANY
            }
        )

    def test_final_ext_priority_remuxvideo(self) -> None:
        """Verify that the final_ext ytdl_opt is set to remuxvideo
        after recodevideo"""
        self._test_custom_opts(
            custom_opts={
                'recodevideo': None,
                'remuxvideo': 'REMUX',
                'extractaudio': {
                    'audioformat': 'EXTRACT'
                }
            },
            loaded_custom_opts={
                'final_ext': 'REMUX',
                'format': mock.ANY,
                'postprocessors': mock.ANY
            }
        )

    def test_final_ext_priority_extractaudio(self) -> None:
        """Verify that the final_ext ytdl_opt is set to
        extractaudio.audioformat last"""
        self._test_custom_opts(
            custom_opts={
                'recodevideo': None,
                'remuxvideo': None,
                'extractaudio': {
                    'audioformat': 'EXTRACT'
                }
            },
            loaded_custom_opts={
                'final_ext': 'EXTRACT',
                'format': mock.ANY,
                'postprocessors': mock.ANY
            }
        )

    def test_final_ext_only_invalid_recodevideo(self) -> None:
        """Verify that the final_ext ytdl_opt isn't set to the value of
        remuxvideo when recodevideo is given but invalid"""
        self._test_custom_opts(
            custom_opts={
                'recodevideo': 'RECODE/INVALID',
                'remuxvideo': 'REMUX',
                'extractaudio': {
                    'audioformat': 'EXTRACT'
                }
            },
            loaded_custom_opts={
                'format': mock.ANY,
                'postprocessors': mock.ANY
            }
        )

    def test_final_ext_only_invalid_remuxvideo(self) -> None:
        """Verify that the final_ext ytdl_opt isn't set to the value of
        extractaudio when remuxvideo is given but invalid"""
        self._test_custom_opts(
            custom_opts={
                'recodevideo': None,
                'remuxvideo': 'REMUX/INVALID',
                'extractaudio': {
                    'audioformat': 'EXTRACT'
                }
            },
            loaded_custom_opts={
                'format': mock.ANY,
                'postprocessors': mock.ANY
            }
        )

    def test_final_ext_existing(self) -> None:
        """Verify that the final_ext ytdl_opt isn't overwritten when
        it's manually set"""
        self._test_custom_opts(
            ytdl_opts={
                'final_ext': 'CUSTOM'
            },
            custom_opts={
                'recodevideo': 'REMUX'
            },
            loaded_custom_opts={
                'postprocessors': mock.ANY
            }
        )

    def test_multiple_custom_opts(self) -> None:
        """Load multiple postprocessor custom_opts at once"""
        self._test_custom_opts(
            custom_opts={
                'convertsubtitles': 'ass',
                'xattrs': True,
                'extractaudio': {
                    'audioformat': 'opus'
                }
            },
            loaded_custom_opts={
                'format': 'bestaudio/best',
                'final_ext': 'opus',
                'postprocessors': [
                    {
                        'key': 'FFmpegSubtitlesConvertor',
                        'format': 'ass'
                    },
                    {
                        'key': 'FFmpegExtractAudio',
                        'preferredcodec': 'opus',
                        'preferredquality': mock.ANY,
                        'nopostoverwrites': mock.ANY
                    },
                    {
                        'key': 'XAttrMetadata'
                    }
                ]
            }
        )
