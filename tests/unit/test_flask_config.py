from __future__ import annotations

from types import SimpleNamespace
from typing import cast, TYPE_CHECKING

from .base import TestBase

if TYPE_CHECKING:
    from typing import Any


class TestFlaskConfig(TestBase):
    def test_basic(self) -> None:
        """Get /config"""
        config = {
            'download_dir': 'DOWNLOAD_DIR',
            'force_download_dir': True,
            'allow_user_opts': True,
            'ytdl_opts': {
                'default_opts': {
                    'opt1': 'str',
                    'opt2': 5
                },
                'enable_whitelist': True,
                'sensitive_opts': ['sensitive'],
                'whitelist': ['opt1', 'opt2'],
                'whitelist_add': [],
                'whitelist_remove': []
            },
            'custom_opts': {
                'default_opts': {
                    'copt1': 4,
                    'copt2': 'custom'
                },
                'enable_whitelist': True,
                'whitelist': ['custom1', 'custom2']
            }
        }
        expected_json = {
            'download_dir': 'DOWNLOAD_DIR',
            'force_download_dir': True,
            'ytdl_default_opts': {
                'opt1': 'str',
                'opt2': 5
            },
            'allow_user_opts': True,
            'ytdl_enable_whitelist': True,
            'ytdl_whitelist': ['opt1', 'opt2'],
            'ytdl_sensitive_opts': ['sensitive'],
            'custom_default_opts': {
                'copt1': 4,
                'copt2': 'custom'
            },
            'custom_enable_whitelist': True,
            'custom_whitelist': ['custom1', 'custom2']
        }
        self.setUpApp(config)
        response = self.client.get('/config')

        with self.subTest(msg='Status code'):
            self.assertEqual(200, response.status_code)
        with self.subTest(msg='Content type'):
            self.assertEqual('application/json', response.content_type)
        with self.subTest(msg='Response data'):
            self.assertEqual(expected_json, response.get_json())

    def test_ytdl_whitelist_null(self) -> None:
        """Get /config when the ytdl whitelist is disabled

        'ytdl_whitelist' is set to `None` when it's not in use
        ('allow_user_opts' and/or 'ytdl_enable_whitelist' is `False`)
        """
        subtests = (
            SimpleNamespace(
                allow_user_opts=True, ytdl_enable_whitelist=True,
                expected_null=False
            ),
            SimpleNamespace(
                allow_user_opts=False, ytdl_enable_whitelist=True,
                expected_null=True
            ),
            SimpleNamespace(
                allow_user_opts=True, ytdl_enable_whitelist=False,
                expected_null=True
            ),
            SimpleNamespace(
                allow_user_opts=False, ytdl_enable_whitelist=False,
                expected_null=True
            )
        )

        for subtest in subtests:
            with self.subTest(subtest):
                config = {
                    'allow_user_opts': subtest.allow_user_opts,
                    'ytdl_opts': {
                        'enable_whitelist': subtest.ytdl_enable_whitelist,
                        'whitelist': ['opt1', 'opt2']
                    }
                }
                self.setUpApp(config)
                response = self.client.get('/config')

                self.assertEqual(200, response.status_code)
                # Override the type for `data`. Other tests verify the
                # typing
                data = cast('dict[str, Any]', response.get_json())

                self.assertIn('ytdl_whitelist', data)
                if subtest.expected_null:
                    self.assertIsNone(data['ytdl_whitelist'])
                else:
                    self.assertEqual(['opt1', 'opt2'], data['ytdl_whitelist'])

    def test_custom_whitelist_null(self) -> None:
        """Get /config when the custom whitelist is disabled

        'custom_whitelist' is set to `None` when it's not in use
        ('allow_user_opts' and/or 'custom_enable_whitelist' is `False`)
        """
        subtests = (
            SimpleNamespace(
                allow_user_opts=True, custom_enable_whitelist=True,
                expected_null=False
            ),
            SimpleNamespace(
                allow_user_opts=False, custom_enable_whitelist=True,
                expected_null=True
            ),
            SimpleNamespace(
                allow_user_opts=True, custom_enable_whitelist=False,
                expected_null=True
            ),
            SimpleNamespace(
                allow_user_opts=False, custom_enable_whitelist=False,
                expected_null=True
            )
        )

        for subtest in subtests:
            with self.subTest(subtest):
                config = {
                    'allow_user_opts': subtest.allow_user_opts,
                    'custom_opts': {
                        'enable_whitelist': subtest.custom_enable_whitelist,
                        'whitelist': ['opt1', 'opt2']
                    }
                }
                self.setUpApp(config)
                response = self.client.get('/config')

                self.assertEqual(200, response.status_code)
                # Override the type for `data`. Other tests verify the
                # typing
                data = cast('dict[str, Any]', response.get_json())

                self.assertIn('custom_whitelist', data)
                if subtest.expected_null:
                    self.assertIsNone(data['custom_whitelist'])
                else:
                    self.assertEqual(
                        ['opt1', 'opt2'], data['custom_whitelist']
                    )

    def test_sensitive_default_opts(self) -> None:
        """Get /config with sensitive default opts

        The 'sensitive' opt should be set to `None`
        """
        config = {
            'allow_user_opts': True,
            'ytdl_opts': {
                'default_opts': {
                    'not-sensitive': 'VALUE',
                    'sensitive': 'VALUE'
                },
                'sensitive_opts': ['sensitive']
            }
        }
        expected_opts = {
            'not-sensitive': 'VALUE',
            'sensitive': None
        }
        self.setUpApp(config)
        response = self.client.get('/config')

        self.assertEqual(200, response.status_code)
        # Override the type for `data`. Other tests verify the typing
        data = cast('dict[str, Any]', response.get_json())

        self.assertIn('ytdl_default_opts', data)
        self.assertEqual(expected_opts, data['ytdl_default_opts'])

    def test_merge_whitelist(self) -> None:
        """Get /config with a merged whitelist"""
        config = {
            'allow_user_opts': True,
            'ytdl_opts': {
                'whitelist': [1, 2, 3],
                'whitelist_add': [4, 5],
                'whitelist_remove': [3, 5]
            }
        }
        expected_whitelist = [1, 2, 4]
        self.setUpApp(config)
        response = self.client.get('/config')

        self.assertEqual(200, response.status_code)
        # Override the type for `data`. Other tests verify the typing
        data = cast('dict[str, Any]', response.get_json())

        self.assertIn('ytdl_whitelist', data)
        self.assertEqual(expected_whitelist, data['ytdl_whitelist'])
