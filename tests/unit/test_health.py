from __future__ import annotations

from .base import TestBase


class TestHealth(TestBase):
    def test_get(self) -> None:
        """Get /health"""
        expected_json = {
            'healthy': True
        }

        self.setUpApp()
        response = self.client.get('/health')

        with self.subTest(msg='Status code'):
            self.assertEqual(200, response.status_code)
        with self.subTest(msg='Content type'):
            self.assertEqual('application/json', response.content_type)
        with self.subTest(msg='Response data'):
            self.assertEqual(expected_json, response.get_json())
