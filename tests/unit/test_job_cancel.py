from __future__ import annotations

import unittest.mock as mock
from typing import cast, TYPE_CHECKING

from ..util import assertValidStatusURL
from .base import TestBase
from ytdl_server.error import http_error, JobNotFoundError
from ytdl_server.util import StatusEnum

if TYPE_CHECKING:
    from typing import Any


class TestJobCancel(TestBase):
    """Tests for cancelling a job"""
    def test_success(self) -> None:
        """Successfully cancel a job"""
        self.setUpApp()
        data = {
            'status': 'cancelled'
        }
        expected_json = {
            'id': str(self.random_uuid),
            'status_url': mock.ANY,
            'description': mock.ANY
        }

        self.patch_job_status(StatusEnum.downloading)
        self.patch_celery_state('PENDING')

        response = self.client.patch(f'/jobs/{self.random_uuid}', json=data)
        self.assertEqual(response.status_code, 202)

        # Override the type for `response_json` because `assertEqual()`
        # ensures that it's the correct type
        response_json = cast('dict[str, Any]', response.get_json())
        self.assertEqual(response_json, expected_json)
        assertValidStatusURL(
            self.random_uuid, response_json['status_url']
        )

    def test_id_not_found(self) -> None:
        """Attempt to cancel a job with a nonexistent ID"""
        self.setUpApp()
        data = {
            'status': 'cancelled'
        }

        # `get_job_status()` raises `JobNotFoundError` when no job is
        # found
        self.mock_db_get_job_status.side_effect = JobNotFoundError(
            self.random_uuid
        )

        response = self.client.patch(f'/jobs/{self.random_uuid}', json=data)
        self.assertEqual(response.status_code, 404)
        self.assertAbortNotCalled()

    def test_no_data(self) -> None:
        """Cancel with no json data"""
        self.setUpApp()

        response = self.client.patch(f'/jobs/{self.random_uuid}')
        self.assertEqual(response.status_code, 400)
        self.assertIsHTTPErrorType(response, http_error.DataBadType)
        self.assertAbortNotCalled()

    def test_wrong_data_type(self) -> None:
        """Cancel with the wrong data type"""
        self.setUpApp()

        response = self.client.patch(
            f'/jobs/{self.random_uuid}', json=['array']
        )
        self.assertEqual(response.status_code, 400)
        self.assertIsHTTPErrorType(response, http_error.DataBadType)
        self.assertAbortNotCalled()

    def test_empty_object(self) -> None:
        """Cancel with an empty JSON object"""
        self.setUpApp()

        response = self.client.patch(f'/jobs/{self.random_uuid}', json={})
        self.assertEqual(response.status_code, 400)
        self.assertAbortNotCalled()

    def test_wrong_status(self) -> None:
        """Cancel with status not set to 'cancelled'"""
        self.setUpApp()
        data = {
            'status': 'finished'
        }

        response = self.client.patch(f'/jobs/{self.random_uuid}', json=data)
        self.assertEqual(response.status_code, 400)
        self.assertAbortNotCalled()

    def test_already_finished(self) -> None:
        """Cancel a job that already finished"""
        self.setUpApp()
        data = {
            'status': 'cancelled'
        }
        self.patch_job_status(StatusEnum.finished)

        response = self.client.patch(f'/jobs/{self.random_uuid}', json=data)
        self.assertEqual(response.status_code, 400)
        self.assertAbortNotCalled()

    def test_already_errored(self) -> None:
        """Cancel a job that failed with the 'error' status"""
        self.setUpApp()
        data = {
            'status': 'cancelled'
        }
        self.patch_job_status(StatusEnum.error)

        response = self.client.patch(f'/jobs/{self.random_uuid}', json=data)
        self.assertEqual(response.status_code, 400)
        self.assertAbortNotCalled()

    def test_already_timed_out(self) -> None:
        """Cancel a job that timed out with the 'timeout' status"""
        self.setUpApp()
        data = {
            'status': 'cancelled'
        }
        self.patch_job_status(StatusEnum.timeout)

        response = self.client.patch(f'/jobs/{self.random_uuid}', json=data)
        self.assertEqual(response.status_code, 400)
        self.assertAbortNotCalled()

    def test_already_cancelled(self) -> None:
        """Cancel a job that was already cancelled"""
        self.setUpApp()
        data = {
            'status': 'cancelled'
        }
        self.patch_job_status(StatusEnum.cancelled)

        response = self.client.patch(f'/jobs/{self.random_uuid}', json=data)
        self.assertEqual(response.status_code, 200)
        self.assertAbortNotCalled()

    def test_already_aborted(self) -> None:
        """Cancel a job where the Celery task has already been aborted
        """
        self.setUpApp()
        data = {
            'status': 'cancelled'
        }
        self.patch_job_status(StatusEnum.downloading)
        self.patch_celery_state('ABORTED')

        response = self.client.patch(f'/jobs/{self.random_uuid}', json=data)
        self.assertEqual(response.status_code, 202)
        self.assertAbortNotCalled()

    def _test_cancel_celery_500(self, celery_state: str) -> None:
        """Assert that it returns 500 when the Celery task is already
        finished, but the SQL job status is still active"""
        self.setUpApp()
        self.disable_logging()
        data = {
            'status': 'cancelled'
        }
        self.patch_job_status(StatusEnum.downloading)
        self.patch_celery_state(celery_state)

        response = self.client.patch(f'/jobs/{self.random_uuid}', json=data)
        self.assertEqual(response.status_code, 500)
        self.assertAbortNotCalled()

    def test_celery_success(self) -> None:
        """Fail to cancel a job that where the Celery task state is
        'SUCCESS'"""
        self._test_cancel_celery_500('SUCCESS')

    def test_celery_failure(self) -> None:
        """Faile to cancel a job that where the Celery task state is
        'FAILURE'"""
        self._test_cancel_celery_500('FAILURE')

    def test_celery_revoked(self) -> None:
        """Faile to cancel a job that where the Celery task state is
        'REVOKED'"""
        self._test_cancel_celery_500('REVOKED')
