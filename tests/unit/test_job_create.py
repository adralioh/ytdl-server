from __future__ import annotations

from typing import cast, TYPE_CHECKING
import os.path
import unittest.mock as mock

from ytdl_server.error import http_error

from ..util import assertValidStatusURL
from .base import TestBase

if TYPE_CHECKING:
    from typing import Any


class TestJobCreate(TestBase):
    """Tests for creating a job"""
    def test_basic_response(self) -> None:
        """Create a basic job and check the response"""
        self.setUpApp()
        data = {
            'urls': ('url1', 'url2', 'url3')
        }
        expected_json = {
            'id': mock.ANY,
            'status_url': mock.ANY,
        }

        response = self.client.post('/jobs/', json=data)
        self.assertEqual(response.status_code, 202)

        # Override the type for `response_json` because `assertEqual()`
        # ensures that it's the correct type
        response_json = cast('dict[str, Any]', response.get_json())
        self.assertEqual(response_json, expected_json)

        job_id = response_json['id']
        self.assertEqual(job_id, str(self.random_uuid))
        assertValidStatusURL(job_id, response_json['status_url'])

    def test_default_config(self) -> None:
        """Create a job with a custom default config"""
        ytdl_default_opts = {
            'outtmpl': '%(uploader)s/%(title)s.%(ext)s',
            'format': 'best',
            'writesubtitles': True
        }
        self._test_create_succeed(
            app_config={
                'ytdl_opts': {
                    'default_opts': ytdl_default_opts
                }
            },
            response_ytdl_opts=ytdl_default_opts
        )

    def test_user_config(self) -> None:
        """Create a job with a custom user config"""
        self._test_create_succeed(
            request_ytdl_opts={
                'outtmpl': '%(uploader)s/%(title)s.%(ext)s',
                'format': 'best',
                'writesubtitles': True
            }
        )

    def test_merge_config(self) -> None:
        """Create a job with default and user configs"""
        ytdl_default_opts = {
            'outtmpl': '%(uploader)s/%(title)s.%(ext)s',
            'format': 'best',
            'writesubtitles': True
        }
        ytdl_user_opts = {
            'writesubtitles': True,
            # Overwrites the format in 'ytdl_default_opts'
            'format': 'worst'
        }
        self._test_create_succeed(
            app_config={
                'ytdl_opts': {
                    'default_opts': ytdl_default_opts
                }
            },
            request_ytdl_opts=ytdl_user_opts,
            response_ytdl_opts=ytdl_default_opts | ytdl_user_opts
        )

    def test_disallow_user_ytdl_opts(self) -> None:
        """Don't allow any user ytdl opts"""
        self._test_create_fail(
            status_code=403,
            app_config={
                'allow_user_opts': False
            },
            request_ytdl_opts={
                'outtmpl': '%(uploader)s/%(title)s.%(ext)s',
            }
        )

    def test_disallow_user_custom_opts(self) -> None:
        """Don't allow any user custom opts"""
        self._test_create_fail(
            status_code=403,
            app_config={
                'allow_user_opts': False
            },
            request_custom_opts={
                'datebefore': '20200101'
            }
        )

    def test_allow_all_opts(self) -> None:
        """Disable the user ytdl opt whitelist"""
        self._test_create_succeed(
            app_config={
                'ytdl_opts': {
                    'enable_whitelist': False,
                    'whitelist_remove': ['TEST_OPTION']
                }
            },
            request_ytdl_opts={
                'TEST_OPTION': True
            }
        )

    def test_blacklisted(self) -> None:
        """Create a job with a non-whitelisted user option"""
        self._test_create_fail(
            status_code=403,
            request_ytdl_opts={
                'TEST_OPTION': True
            }
        )

    def test_default_ignores_whitelist(self) -> None:
        """Verify the default config ignores the whitelist"""
        self._test_create_succeed(
            app_config={
                'ytdl_opts': {
                    'default_opts': {
                        'TEST_OPTION': True
                    }
                }
            },
            response_ytdl_opts={
                'TEST_OPTION': True
            }
        )

    def test_default_custom_config(self) -> None:
        """Create a job with a custom custom_default_opts config"""
        custom_default_opts = {
            'embedsubtitles': True,
            'extractaudio': {
                'audioformat': 'opus'
            }
        }
        self._test_create_succeed(
            app_config={
                'custom_opts': {
                    'default_opts': custom_default_opts
                }
            },
            response_custom_opts=custom_default_opts
        )

    def test_merge_custom_config(self) -> None:
        """Create a job with default and user custom_opts"""
        custom_default_opts = {
            'embedsubtitles': True,
            'extractaudio': {
                'audioformat': 'opus'
            }
        }
        custom_user_opts = {
            'addmetadata': True,
            # Overwrites the format in 'custom_default_opts'
            'extractaudio': {
                'audioformat': 'mp3',
                'audioquality': 0
            }
        }
        self._test_create_succeed(
            app_config={
                'custom_opts': {
                    'default_opts': custom_default_opts
                }
            },
            request_custom_opts=custom_user_opts,
            response_custom_opts=custom_default_opts | custom_user_opts
        )

    def test_default_custom_ignores_whitelist(self) -> None:
        """Verify the default custom_opts ignore the whitelist"""
        self._test_create_succeed(
            app_config={
                'custom_opts': {
                    'default_opts': {
                        'TEST_OPTION': True
                    },
                    'enable_whitelist': True,
                    'whitelist': []
                }
            },
            response_custom_opts={
                'TEST_OPTION': True
            }
        )

    def test_whitelist_add(self) -> None:
        """Verify `ytdl_whitelist_add` works"""
        self._test_create_succeed(
            app_config={
                'ytdl_opts': {
                    'whitelist_add': ['TEST_OPTION']
                }
            },
            request_ytdl_opts={
                'TEST_OPTION': True
            }
        )

    def test_whitelist_remove(self) -> None:
        """Verify `ytdl_whitelist_remove` works"""
        self._test_create_fail(
            status_code=403,
            app_config={
                'ytdl_opts': {
                    'whitelist': ['TEST_OPTION'],
                    'whitelist_remove': ['TEST_OPTION']
                }
            },
            request_ytdl_opts={
                'TEST_OPTION': True
            }
        )

    def test_whitelist_remove_overrides_add(self) -> None:
        """`ytdl_whitelist_remove` overrides `ytdl_whitelist_add`
        """
        self._test_create_fail(
            status_code=403,
            app_config={
                'ytdl_opts': {
                    'whitelist_add': ['TEST_OPTION'],
                    'whitelist_remove': ['TEST_OPTION']
                }
            },
            request_ytdl_opts={
                'TEST_OPTION': True
            }
        )

    def test_custom_opts_blacklisted(self) -> None:
        """Attempt to use a non-whitelisted custom_opt"""
        self._test_create_fail(
            status_code=403,
            app_config={
                'custom_opts': {
                    'enable_whitelist': True,
                    'whitelist': []
                }
            },
            request_custom_opts={
                'TEST_OPTION': True
            }
        )

    def test_custom_opts_whitelisted(self) -> None:
        """Use a whitelisted custom_opt"""
        self._test_create_succeed(
            app_config={
                'custom_opts': {
                    'enable_whitelist': True,
                    'whitelist': ['TEST_OPTION']
                }
            },
            request_custom_opts={
                'TEST_OPTION': True
            }
        )

    def test_download_dir_force_fail_absolute(self) -> None:
        """`force_download_dir` - child dir not in parent"""
        self.setUpTempDir()
        self._test_create_fail(
            status_code=403,
            app_config={
                'download_dir': self.temp_dir,
                'force_download_dir': True
            },
            request_ytdl_opts={
                'outtmpl': '/not-parent/%(title)s.%(ext)s',
            }
        )

    def test_download_dir_force_fail_dots(self) -> None:
        """`force_download_dir` - escape parent via '..'"""
        self.setUpTempDir()
        self._test_create_fail(
            status_code=403,
            app_config={
                'download_dir': self.temp_dir,
                'force_download_dir': True
            },
            request_ytdl_opts={
                'outtmpl': os.path.join(self.temp_dir, '../%(title)s.%(ext)s')
            }
        )

    def test_download_dir_force_valid(self) -> None:
        """`force_download_dir` - child dir in parent"""
        self.setUpTempDir()
        self._test_create_succeed(
            app_config={
                'download_dir': self.temp_dir,
                'force_download_dir': True
            },
            request_ytdl_opts={
                'outtmpl': os.path.join(self.temp_dir, '%(title)s.%(ext)s')
            }
        )

    def test_outtmpl_na_placeholder_valid(self) -> None:
        """outtmpl_na_placeholder works when it doesn't contain a slash
        """
        self._test_create_succeed(
            app_config={
                'ytdl_opts': {
                    'whitelist': ['outtmpl_na_placeholder']
                }
            },
            request_ytdl_opts={
                'outtmpl_na_placeholder': 'NA'
            }
        )

    def test_outtmpl_na_placeholder_slash(self) -> None:
        """outtmpl_na_placeholder is forbidden when it contains a slash
        """
        self._test_create_fail(
            status_code=403,
            app_config={
                'ytdl_opts': {
                    'whitelist': ['outtmpl_na_placeholder']
                }
            },
            request_ytdl_opts={
                'outtmpl_na_placeholder': 'N/A'
            }
        )

    def test_merge_output_format_valid(self) -> None:
        """merge_output_format works when it doesn't contain a slash"""
        self._test_create_succeed(
            app_config={
                'ytdl_opts': {
                    'whitelist': ['merge_output_format']
                }
            },
            request_ytdl_opts={
                'merge_output_format': 'mkv'
            }
        )

    def test_merge_output_format_slash(self) -> None:
        """merge_output_format is forbidden when it contains a slash"""
        self._test_create_fail(
            status_code=403,
            app_config={
                'ytdl_opts': {
                    'whitelist': ['merge_output_format']
                }
            },
            request_ytdl_opts={
                'merge_output_format': 'm/kv'
            }
        )

    def test_final_ext_valid(self) -> None:
        """final_ext works when it doesn't contain a slash"""
        self._test_create_succeed(
            app_config={
                'ytdl_opts': {
                    'whitelist': ['final_ext']
                }
            },
            request_ytdl_opts={
                'final_ext': 'mkv'
            }
        )

    def test_final_ext_slash(self) -> None:
        """final_ext is forbidden when it contains a slash"""
        self._test_create_fail(
            status_code=403,
            app_config={
                'ytdl_opts': {
                    'whitelist': ['final_ext']
                }
            },
            request_ytdl_opts={
                'final_ext': 'mk/v'
            }
        )

    def test_custom_opt_daterange(self) -> None:
        """custom_opts daterange options"""
        self._test_create_succeed(
            request_custom_opts={
                'dateafter': '20200101',
                'datebefore': '20200201'
            }
        )

    def test_custom_opt_daterange_none(self) -> None:
        """custom_opts daterange options set to `None`"""
        self._test_create_succeed(
            request_custom_opts={
                'dateafter': None,
                'datebefore': None
            }
        )

    def test_custom_opt_datebefore_wrong_type(self) -> None:
        """custom_opts datebefore has the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'datebefore': 1234
            }
        )

    def test_custom_opt_dateafter_wrong_type(self) -> None:
        """custom_opts dateafter has the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'dateafter': 1234
            }
        )

    def test_custom_opt_metafromtitle(self) -> None:
        """custom_opts metafromtitle"""
        self._test_create_succeed(
            request_custom_opts={
                'metafromtitle': '%(artist)s - %(title)s'
            }
        )

    def test_custom_opt_metafromtitle_wrong_type(self) -> None:
        """custom_opts metafromtitle wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'metafromtitle': 1
            }
        )

    def test_custom_opt_extractaudio(self) -> None:
        """custom_opts extractaudio empty dict"""
        self._test_create_succeed(
            request_custom_opts={
                'extractaudio': {}
            }
        )

    def test_custom_opt_extractaudio_none(self) -> None:
        """custom_opts extractaudio is None

        Equivalent to not passing it
        """
        self._test_create_succeed(
            request_custom_opts={
                'extractaudio': None
            }
        )

    def test_custom_opt_extractaudio_wrong_type(self) -> None:
        """custom_opts extractaudio wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'extractaudio': 'string'
            }
        )

    def test_custom_opt_extractaudio_fields(self) -> None:
        """custom_opts extractaudio fields"""
        self._test_create_succeed(
            request_custom_opts={
                'extractaudio': {
                    'audioformat': 'mp3',
                    'audioquality': 320,
                    'nopostoverwrites': True
                }
            }
        )

    def test_custom_opt_extractaudio_audioformat_wrong_type(self) -> None:
        """custom_opts extractaudio fields with wrong types"""
        self._test_create_fail(
            request_custom_opts={
                'extractaudio': {
                    'audioformat': []
                }
            }
        )

    def test_custom_opt_extractaudio_audioquality_wrong_type(self) -> None:
        """custom_opts extractaudio fields with wrong types"""
        self._test_create_fail(
            request_custom_opts={
                'extractaudio': {
                    'audioquality': []
                }
            }
        )

    def test_custom_opt_extractaudio_nopostoverwrites_wrong_type(self) -> None:
        """custom_opts extractaudio fields with wrong types"""
        self._test_create_fail(
            request_custom_opts={
                'extractaudio': {
                    'nopostoverwrites': []
                }
            }
        )

    def test_custom_opt_extractaudio_audioquality_0(self) -> None:
        """custom_opts extractaudio - minimum audio quality"""
        self._test_create_succeed(
            request_custom_opts={
                'extractaudio': {
                    'audioquality': 0
                }
            }
        )

    def test_custom_opt_extractaudio_audioquality_negative(self) -> None:
        """custom_opts extractaudio - negative audio quality"""
        self._test_create_fail(
            request_custom_opts={
                'extractaudio': {
                    'audioquality': -1
                }
            }
        )

    def test_custom_opt_remuxvideo(self) -> None:
        """custom_opts remuxvideo"""
        self._test_create_succeed(
            request_custom_opts={
                'remuxvideo': 'mkv'
            }
        )

    def test_custom_opt_remuxvideo_wrong_type(self) -> None:
        """custom_opts remuxvideo wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'remuxvideo': 1
            }
        )

    def test_custom_opt_remuxvideo_path_sep(self) -> None:
        """custom_opts remuxvideo has a path sep when download dir is
        forced"""
        self._test_create_fail(
            status_code=403,
            app_config={
                'force_download_dir': True
            },
            request_custom_opts={
                'remuxvideo': 'path/../sep'
            }
        )

    def test_custom_opt_remuxvideo_path_sep_not_forced(self) -> None:
        """custom_opts remuxvideo has a path sep when download dir
        isn't forced"""
        self._test_create_succeed(
            app_config={
                'force_download_dir': False
            },
            request_custom_opts={
                'remuxvideo': 'path/../sep'
            }
        )

    def test_custom_opt_recodevideo(self) -> None:
        """custom_opts recodevideo"""
        self._test_create_succeed(
            request_custom_opts={
                'recodevideo': 'mkv'
            }
        )

    def test_custom_opt_recodevideo_wrong_type(self) -> None:
        """custom_opts recodevideo wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'recodevideo': 1
            }
        )

    def test_custom_opt_recodevideo_path_sep(self) -> None:
        """custom_opts recodevideo has a path sep when download dir is
        forced"""
        self._test_create_fail(
            status_code=403,
            app_config={
                'force_download_dir': True
            },
            request_custom_opts={
                'recodevideo': 'path/../sep'
            }
        )

    def test_custom_opt_recodevideo_path_sep_not_forced(self) -> None:
        """custom_opts recodevideo has a path sep when download dir
        isn't forced"""
        self._test_create_succeed(
            app_config={
                'force_download_dir': False
            },
            request_custom_opts={
                'recodevideo': 'path/../sep'
            }
        )

    def test_custom_opt_addmetadata(self) -> None:
        """custom_opts addmetadata"""
        self._test_create_succeed(
            request_custom_opts={
                'addmetadata': True
            }
        )

    def test_custom_opt_addmetadata_wrong_type(self) -> None:
        """custom_opts addmetadata wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'addmetadata': 'string'
            }
        )

    def test_custom_opt_addchapters(self) -> None:
        """custom_opts addchapters"""
        self._test_create_succeed(
            request_custom_opts={
                'addchapters': True
            }
        )

    def test_custom_opt_addchapters_wrong_type(self) -> None:
        """custom_opts addchapters wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'addchapters': 'string'
            }
        )

    def test_custom_opt_convertsubtitles(self) -> None:
        """custom_opts convertsubtitles"""
        self._test_create_succeed(
            request_custom_opts={
                'convertsubtitles': 'ass'
            }
        )

    def test_custom_opt_convertsubtitles_wrong_type(self) -> None:
        """custom_opts convertsubtitles wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'convertsubtitles': 1
            }
        )

    def test_custom_opt_convertsubtitles_path_sep(self) -> None:
        """custom_opts convertsubtitles has a path sep when download dir is
        forced"""
        self._test_create_fail(
            status_code=403,
            app_config={
                'force_download_dir': True
            },
            request_custom_opts={
                'convertsubtitles': 'path/../sep'
            }
        )

    def test_custom_opt_convertsubtitles_path_sep_not_forced(self) -> None:
        """custom_opts convertsubtitles has a path sep when download dir
        isn't forced"""
        self._test_create_succeed(
            app_config={
                'force_download_dir': False
            },
            request_custom_opts={
                'convertsubtitles': 'path/../sep'
            }
        )

    def test_custom_opt_convertthumbnails(self) -> None:
        """custom_opts convertthumbnails"""
        self._test_create_succeed(
            request_custom_opts={
                'convertthumbnails': 'ass'
            }
        )

    def test_custom_opt_convertthumbnails_wrong_type(self) -> None:
        """custom_opts convertthumbnails wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'convertthumbnails': 1
            }
        )

    def test_custom_opt_convertthumbnails_path_sep(self) -> None:
        """custom_opts convertthumbnails has a path sep when download dir is
        forced"""
        self._test_create_fail(
            status_code=403,
            app_config={
                'force_download_dir': True
            },
            request_custom_opts={
                'convertthumbnails': 'path/../sep'
            }
        )

    def test_custom_opt_convertthumbnails_path_sep_not_forced(self) -> None:
        """custom_opts convertthumbnails has a path sep when download dir
        isn't forced"""
        self._test_create_succeed(
            app_config={
                'force_download_dir': False
            },
            request_custom_opts={
                'convertthumbnails': 'path/../sep'
            }
        )

    def test_custom_opt_embedsubtitles(self) -> None:
        """custom_opts embedsubtitles"""
        self._test_create_succeed(
            request_custom_opts={
                'embedsubtitles': True
            }
        )

    def test_custom_opt_embedsubtitles_wrong_type(self) -> None:
        """custom_opts embedsubtitles wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'embedsubtitles': 'string'
            }
        )

    def test_custom_opt_embedthumbnail(self) -> None:
        """custom_opts embedthumbnail"""
        self._test_create_succeed(
            request_custom_opts={
                'embedthumbnail': True
            }
        )

    def test_custom_opt_embedthumbnail_wrong_type(self) -> None:
        """custom_opts embedthumbnail wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'embedthumbnail': 'string'
            }
        )

    def test_custom_opt_xattrs(self) -> None:
        """custom_opts xattrs"""
        self._test_create_succeed(
            request_custom_opts={
                'xattrs': True
            }
        )

    def test_custom_opt_xattrs_wrong_type(self) -> None:
        """custom_opts xattrs wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'xattrs': 'string'
            }
        )

    def test_custom_opt_parse_metadata(self) -> None:
        """custom_opts parse_metadata"""
        self._test_create_succeed(
            request_custom_opts={
                'parse_metadata': [
                    {'from': 'FROM1', 'to': 'TO1'},
                    {'from': 'FROM2', 'to': 'TO2'},
                ]
            }
        )

    def test_custom_opt_parse_metadata_empty_list(self) -> None:
        """custom_opts parse_metadata empty list"""
        self._test_create_succeed(
            request_custom_opts={
                'parse_metadata': []
            }
        )

    def test_custom_opt_parse_metadata_wrong_type(self) -> None:
        """custom_opts parse_metadata wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'parse_metadata': 'str'
            }
        )

    def test_custom_opt_parse_metadata_interpreter_wrong_type(self) -> None:
        """custom_opts parse_metadata interpreter is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'parse_metadata': [123]
            }
        )

    def test_custom_opt_parse_metadata_missing_field_from(self) -> None:
        """custom_opts parse_metadata missing 'from' field"""
        self._test_create_fail(
            request_custom_opts={
                'parse_metadata': [
                    {'to': 'TO'}
                ]
            }
        )

    def test_custom_opt_parse_metadata_missing_field_to(self) -> None:
        """custom_opts parse_metadata missing 'to' field"""
        self._test_create_fail(
            request_custom_opts={
                'parse_metadata': [
                    {'from': 'FROM'}
                ]
            }
        )

    def test_custom_opt_parse_metadata_from_wrong_type(self) -> None:
        """custom_opts parse_metadata: 'from' field has wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'parse_metadata': [
                    {'from': 1, 'to': 'TO'}
                ]
            }
        )

    def test_custom_opt_parse_metadata_to_wrong_type(self) -> None:
        """custom_opts parse_metadata: 'to' field has wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'parse_metadata': [
                    {'from': 'FROM', 'to': 1}
                ]
            }
        )

    def test_custom_opt_replace_in_metadata(self) -> None:
        """custom_opts replace_in_metadata"""
        self._test_create_succeed(
            request_custom_opts={
                'replace_in_metadata': [
                    {
                        'fields': ['FIELD1', 'FIELD2'],
                        'regex': 'REGEX1', 'replace': 'REPLACE1'
                    },
                    {
                        'fields': ['FIELD3', 'FIELD4'],
                        'regex': 'REGEX2', 'replace': 'REPLACE2'
                    }
                ]
            }
        )

    def test_custom_opt_replace_in_metadata_empty_list(self) -> None:
        """custom_opts replace_in_metadata empty list"""
        self._test_create_succeed(
            request_custom_opts={
                'replace_in_metadata': []
            }
        )

    def test_custom_opt_replace_in_metadata_wrong_type(self) -> None:
        """custom_opts replace_in_metadata wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'replace_in_metadata': 'str'
            }
        )

    def test_custom_opt_replace_in_metadata_replacer_wrong_type(self) -> None:
        """custom_opts replace_in_metadata replacer is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'replace_in_metadata': [123]
            }
        )

    def test_custom_opt_replace_in_metadata_missing_field_fields(self) -> None:
        """custom_opts replace_in_metadata missing 'fields' field"""
        self._test_create_fail(
            request_custom_opts={
                'replace_in_metadata': [
                    {
                        'regex': 'REGEX1', 'replace': 'REPLACE1'
                    }
                ]
            }
        )

    def test_custom_opt_replace_in_metadata_missing_field_regex(self) -> None:
        """custom_opts replace_in_metadata missing 'regex' field"""
        self._test_create_fail(
            request_custom_opts={
                'replace_in_metadata': [
                    {
                        'fields': ['FIELD1', 'FIELD2'],
                        'replace': 'REPLACE1'
                    }
                ]
            }
        )

    def test_custom_opt_replace_in_metadata_missing_field_replace(
        self
    ) -> None:
        """custom_opts replace_in_metadata missing 'replace' field"""
        self._test_create_fail(
            request_custom_opts={
                'replace_in_metadata': [
                    {
                        'fields': ['FIELD1', 'FIELD2'],
                        'regex': 'REGEX1',
                    }
                ]
            }
        )

    def test_custom_opt_replace_in_metadata_fields_wrong_type(self) -> None:
        """custom_opts replace_in_metadata: 'fields' field has wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'replace_in_metadata': [
                    {
                        'fields': 1,
                        'regex': 'REGEX1', 'replace': 'REPLACE1'
                    }
                ]
            }
        )

    def test_custom_opt_replace_in_metadata_regex_wrong_type(self) -> None:
        """custom_opts replace_in_metadata: 'regex' field has wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'replace_in_metadata': [
                    {
                        'fields': ['FIELD1', 'FIELD2'],
                        'regex': True, 'replace': 'REPLACE1'
                    }
                ]
            }
        )

    def test_custom_opt_replace_in_metadata_replace_wrong_type(self) -> None:
        """custom_opts replace_in_metadata: 'replace' field has wrong
        type"""
        self._test_create_fail(
            request_custom_opts={
                'replace_in_metadata': [
                    {
                        'fields': ['FIELD1', 'FIELD2'],
                        'regex': 'REGEX1', 'replace': []
                    }
                ]
            }
        )

    def test_custom_opt_replace_in_metadata_fields_empty_list(self) -> None:
        """custom_opts replace_in_metadata: 'fields' field is an empty
        list"""
        self._test_create_succeed(
            request_custom_opts={
                'replace_in_metadata': [
                    {
                        'fields': [],
                        'regex': 'REGEX1', 'replace': 'REPLACE1'
                    }
                ]
            }
        )

    def test_custom_opt_replace_in_metadata_fields_sub_wrong_type(
        self
    ) -> None:
        """custom_opts replace_in_metadata: 'fields' field contains a
        value that's the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'replace_in_metadata': [
                    {
                        'fields': ['FIELD1', 1, 'FIELD2'],
                        'regex': 'REGEX1', 'replace': 'REPLACE1'
                    }
                ]
            }
        )

    def test_custom_opt_remove_chapters(self) -> None:
        """custom_opts remove_chapters"""
        self._test_create_succeed(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': [[0, 60], [110.5, 120.9]],
                    'force_keyframes': True
                }
            }
        )

    def test_custom_opt_remove_chapters_none(self) -> None:
        """custom_opts remove_chapters is None"""
        self._test_create_succeed(
            request_custom_opts={
                'remove_chapters': None
            }
        )

    def test_custom_opt_remove_chapters_wrong_type(self) -> None:
        """custom_opts remove_chapters is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': []
            }
        )

    def test_custom_opt_remove_chapters_missing_fields(self) -> None:
        """custom_opts remove_chapters is missing both required fields"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'force_keyframes': False
                }
            }
        )

    def test_custom_opt_remove_chapters_only_patterns(self) -> None:
        """custom_opts remove_chapters is only has the patterns field"""
        self._test_create_succeed(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2']
                }
            }
        )

    def test_custom_opt_remove_chapters_only_ranges(self) -> None:
        """custom_opts remove_chapters is only has the ranges field"""
        self._test_create_succeed(
            request_custom_opts={
                'remove_chapters': {
                    'ranges': [[0, 60], [110.5, 120.9]]
                }
            }
        )

    def test_custom_opt_remove_chapters_patterns_none(self) -> None:
        """custom_opts remove_chapters patterns is None"""
        self._test_create_succeed(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': None,
                    'ranges': [[0, 60], [110.5, 120.9]]
                }
            }
        )

    def test_custom_opt_remove_chapters_ranges_none(self) -> None:
        """custom_opts remove_chapters ranges is None"""
        self._test_create_succeed(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': None
                }
            }
        )

    def test_custom_opt_remove_chapters_both_none(self) -> None:
        """custom_opts remove_chapters patterns and ranges are both None"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': None,
                    'ranges': None
                }
            }
        )

    def test_custom_opt_remove_chapters_both_empty(self) -> None:
        """custom_opts remove_chapters patterns and ranges are both
        empty lists"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': [],
                    'ranges': []
                }
            }
        )

    def test_custom_opt_remove_chapters_patterns_wrong_type(self) -> None:
        """custom_opts remove_chapters patterns is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': 'str',
                    'ranges': [[0, 60], [110.5, 120.9]]
                }
            }
        )

    def test_custom_opt_remove_chapters_patterns_has_wrong_type(self) -> None:
        """custom_opts remove_chapters patterns contains an item with
        the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 5],
                    'ranges': [[0, 60], [110.5, 120.9]]
                }
            }
        )

    def test_custom_opt_remove_chapters_patterns_invalid_regex(self) -> None:
        """custom_opts remove_chapters patterns contains an invalid regex"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', '(missing_paren'],
                    'ranges': [[0, 60], [110.5, 120.9]]
                }
            }
        )

    def test_custom_opt_remove_chapters_ranges_wrong_type(self) -> None:
        """custom_opts remove_chapters range is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': 2
                }
            }
        )

    def test_custom_opt_remove_chapters_ranges_has_wrong_type(self) -> None:
        """custom_opts remove_chapters ranges contains an item with
        the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': [{}, [110.5, 120.9]]
                }
            }
        )

    def test_custom_opt_remove_chapters_ranges_wrong_length(self) -> None:
        """custom_opts remove_chapters ranges contains an item with
        the wrong length"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': [[0, 60, 120]]
                }
            }
        )

    def test_custom_opt_remove_chapters_ranges_start_wrong_type(self) -> None:
        """custom_opts remove_chapters ranges start is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': [[0, 'str']]
                }
            }
        )

    def test_custom_opt_remove_chapters_ranges_end_wrong_type(self) -> None:
        """custom_opts remove_chapters ranges end is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': [['str', 60]]
                }
            }
        )

    def test_custom_opt_remove_chapters_ranges_start_negative(self) -> None:
        """custom_opts remove_chapters ranges start is negative"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': [[-1, 60]]
                }
            }
        )

    def test_custom_opt_remove_chapters_ranges_end_negative(self) -> None:
        """custom_opts remove_chapters ranges end is negative"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': [[0, -1]]
                }
            }
        )

    def test_custom_opt_remove_chapters_ranges_start_ge_end(self) -> None:
        """custom_opts remove_chapters ranges start>=end"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': [[60, 0]]
                }
            }
        )

    def test_custom_opt_remove_chapters_ranges_start_equals_end(self) -> None:
        """custom_opts remove_chapters ranges start==end"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': [[60, 60]]
                }
            }
        )

    def test_custom_opt_remove_chapters_force_keyframes_wrong_type(
        self
    ) -> None:
        """custom_opts remove_chapters force_keyframes is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': [[0, 60]],
                    'force_keyframes': 5
                }
            }
        )

    def test_custom_opt_remove_chapters_force_keyframes_none(
        self
    ) -> None:
        """custom_opts remove_chapters force_keyframes is None"""
        self._test_create_succeed(
            request_custom_opts={
                'remove_chapters': {
                    'patterns': ['REGEX1', 'REGEX2'],
                    'ranges': [[0, 60]],
                    'force_keyframes': None
                }
            }
        )

    def test_custom_opt_split_chapters(self) -> None:
        """custom_opts split_chapters"""
        self._test_create_succeed(
            request_custom_opts={
                'split_chapters': {
                    'force_keyframes': True
                }
            }
        )

    def test_custom_opt_split_chapters_empty(self) -> None:
        """custom_opts split_chapters is an empty dict"""
        self._test_create_succeed(
            request_custom_opts={
                'split_chapters': {}
            }
        )

    def test_custom_opt_split_chapters_none(self) -> None:
        """custom_opts split_chapters is None"""
        self._test_create_succeed(
            request_custom_opts={
                'split_chapters': None
            }
        )

    def test_custom_opt_split_chapters_force_keyframes_wrong_type(
        self
    ) -> None:
        """custom_opts split_chapters force_keyframes is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'split_chapters': {
                    'force_keyframes': 12
                }
            }
        )

    def test_custom_opt_sponsorblock(self) -> None:
        """custom_opts sponsorblock all fields"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1', 'MARK2'],
                    'remove': ['REMOVE1', 'REMOVE2'],
                    'template': 'TEMPLATE',
                    'api': 'API',
                    'force_keyframes': True
                }
            }
        )

    def test_custom_opt_sponsorblock_missing_optional(self) -> None:
        """custom_opts sponsorblock missing optional fields"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1', 'MARK2'],
                    'remove': ['REMOVE1', 'REMOVE2']
                }
            }
        )

    def test_custom_opt_sponsorblock_none(self) -> None:
        """custom_opts sponsorblock is None"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': None
            }
        )

    def test_custom_opt_sponsorblock_wrong_type(self) -> None:
        """custom_opts sponsorblock is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'sponsorblock': []
            }
        )

    def test_custom_opt_sponsorblock_missing_required_fields(self) -> None:
        """custom_opts sponsorblock is missing both required fields"""
        self._test_create_fail(
            request_custom_opts={
                'sponsorblock': {}
            }
        )

    def test_custom_opt_sponsorblock_missing_mark(self) -> None:
        """custom_opts sponsorblock is missing the mark field"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': {
                    'remove': ['REMOVE1', 'REMOVE2']
                }
            }
        )

    def test_custom_opt_sponsorblock_missing_remove(self) -> None:
        """custom_opts sponsorblock is missing the remove field"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1', 'MARK2']
                }
            }
        )

    def test_custom_opt_sponsorblock_both_none(self) -> None:
        """custom_opts sponsorblock both required fields are None"""
        self._test_create_fail(
            request_custom_opts={
                'sponsorblock': {
                    'mark': None,
                    'remove': None
                }
            }
        )

    def test_custom_opt_sponsorblock_mark_none(self) -> None:
        """custom_opts sponsorblock mark field is None"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': {
                    'mark': None,
                    'remove': ['REMOVE1', 'REMOVE2']
                }
            }
        )

    def test_custom_opt_sponsorblock_remove_none(self) -> None:
        """custom_opts sponsorblock remove field is None"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1', 'MARK2'],
                    'remove': None
                }
            }
        )

    def test_custom_opt_sponsorblock_mark_empty(self) -> None:
        """custom_opts sponsorblock mark field is an empty list"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': {
                    'mark': [],
                    'remove': ['REMOVE1', 'REMOVE2']
                }
            }
        )

    def test_custom_opt_sponsorblock_remove_empty(self) -> None:
        """custom_opts sponsorblock remove field is an empty list"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1', 'MARK2'],
                    'remove': []
                }
            }
        )

    def test_custom_opt_sponsorblock_both_empty(self) -> None:
        """custom_opts sponsorblock both required fields are an empty
        list"""
        self._test_create_fail(
            request_custom_opts={
                'sponsorblock': {
                    'mark': [],
                    'remove': []
                }
            }
        )

    def test_custom_opt_sponsorblock_mark_has_wrong_type(self) -> None:
        """custom_opts sponsorblock mark field contains an item with the
        wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1', 123],
                }
            }
        )

    def test_custom_opt_sponsorblock_remove_has_wrong_type(self) -> None:
        """custom_opts sponsorblock remove field contains an item with
        the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'sponsorblock': {
                    'remove': ['REMOVE1', 123],
                }
            }
        )

    def test_custom_opt_sponsorblock_template_none(self) -> None:
        """custom_opts sponsorblock template field is None"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1'],
                    'template': None
                }
            }
        )

    def test_custom_opt_sponsorblock_api_none(self) -> None:
        """custom_opts sponsorblock api field is None"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1'],
                    'api': None
                }
            }
        )

    def test_custom_opt_sponsorblock_template_wrong_type(self) -> None:
        """custom_opts sponsorblock template field is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1'],
                    'template': 123
                }
            }
        )

    def test_custom_opt_sponsorblock_api_wrong_type(self) -> None:
        """custom_opts sponsorblock api field is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1'],
                    'api': 123
                }
            }
        )

    def test_custom_opt_sponsorblock_force_keyframes_none(self) -> None:
        """custom_opts sponsorblock force_keyframes is None"""
        self._test_create_succeed(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1'],
                    'force_keyframes': None
                }
            }
        )

    def test_custom_opt_sponsorblock_force_keyframes_wrong_type(self) -> None:
        """custom_opts sponsorblock force_keyframes is the wrong type"""
        self._test_create_fail(
            request_custom_opts={
                'sponsorblock': {
                    'mark': ['MARK1'],
                    'force_keyframes': 123
                }
            }
        )

    def test_no_data(self) -> None:
        """Create job with no JSON data

        Should return 400
        """
        self.setUpApp()

        response = self.client.post('/jobs/')
        self.assertEqual(response.status_code, 400)
        self.assertIsHTTPErrorType(response, http_error.DataBadType)
        self.assertTaskNotCalled()

    def test_empty_dict(self) -> None:
        """Create job with an empty dict as the JSON data

        Should return 400
        """
        self.setUpApp()
        response = self.client.post('/jobs/', json={})
        self.assertEqual(response.status_code, 400)
        self.assertTaskNotCalled()

    def test_wrong_data_type(self) -> None:
        """Create job with the wrong JSON type

        Should return 400
        """
        self.setUpApp()

        response = self.client.post('/jobs/', json=['list', 'data'])
        self.assertEqual(response.status_code, 400)
        self.assertIsHTTPErrorType(response, http_error.DataBadType)
        self.assertTaskNotCalled()

    def test_empty_url(self) -> None:
        """Create job with an empty urls array

        Should return 400
        """
        self.setUpApp()
        data: dict[str, Any] = {
            'urls': []
        }
        response = self.client.post('/jobs/', json=data)
        self.assertEqual(response.status_code, 400)
        self.assertTaskNotCalled()

    def test_url_wrong_type(self) -> None:
        """Create job with the urls array set to the wrong type

        Should return 400
        """
        self.setUpApp()
        data = {
            'urls': 'not a list'
        }
        response = self.client.post('/jobs/', json=data)
        self.assertEqual(response.status_code, 400)
        self.assertTaskNotCalled()

    def test_ytdl_opt_wrong_type(self) -> None:
        """Create job with the ytdl_opts object set to the wrong type

        Should return 400
        """
        self.setUpApp()
        data = {
            'urls': ('url1', 'url2', 'url3'),
            'ytdl_opts': 123
        }
        response = self.client.post('/jobs/', json=data)
        self.assertEqual(response.status_code, 400)
        self.assertTaskNotCalled()

    def test_custom_opt_wrong_type(self) -> None:
        """Create job with the custom_opts object set to the wrong type

        Should return 400
        """
        self.setUpApp()
        data = {
            'urls': ('url1', 'url2', 'url3'),
            'custom_opts': 123
        }
        response = self.client.post('/jobs/', json=data)
        self.assertEqual(response.status_code, 400)
        self.assertTaskNotCalled()

    def test_wrong_content_type(self) -> None:
        """Create job with the wrong content type

        Should return 400
        """
        self.setUpApp()
        data = {
            'title': '<Title>',
            'text': 'HTML data'
        }

        response = self.client.post('/jobs/', data=data)
        self.assertEqual(response.status_code, 400)
        self.assertIsHTTPErrorType(response, http_error.DataBadType)
        self.assertTaskNotCalled()
