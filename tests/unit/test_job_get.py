from __future__ import annotations

from datetime import datetime

from .base import TestBase
from ytdl_server.error import JobNotFoundError
from ytdl_server.util import Job, Log, LogEnum, Progress, StatusEnum


class TestJobGet(TestBase):
    """Tests for getting job status"""
    def test_not_found(self) -> None:
        """Attempt to get a job that doesn't exist"""
        self.setUpApp()

        # `get_job()` raises `JobNotFoundError` when no job is found
        self.mock_db_get_job.side_effect = JobNotFoundError(self.random_uuid)

        response = self.client.get(f'/jobs/{self.random_uuid}')
        self.assertEqual(response.status_code, 404)

    def test_queued(self) -> None:
        """Get a basic job that has just been created"""
        self.setUpApp()

        job = Job(
            id=self.random_uuid,
            status=StatusEnum.queued,
            urls=('url1', 'url2', 'url3')
        )
        log: tuple[Log, ...] = tuple()
        progress: tuple[Progress, ...] = tuple()

        expected_json = {
            'id': str(self.random_uuid),
            'status': 'queued',
            'urls': ['url1', 'url2', 'url3'],
            'ytdl_opts': None,
            'started': None,
            'finished': None,
            'error': None,
            'progress': [],
            'logs': []
        }

        self.patch_job(job)
        self.patch_log(log)
        self.patch_progress(progress)

        response = self.client.get(f'/jobs/{self.random_uuid}')
        self.assertEqual(response.status_code, 200)

        response_json = response.get_json()
        self.assertEqual(response_json, expected_json)

    def test_full(self) -> None:
        """Get a job that populates all fields in the return object"""
        self.setUpApp()

        job = Job(
            id=self.random_uuid,
            status=StatusEnum.finished,
            urls=['url1', 'url2', 'url3'],
            ytdl_opts={
                'opt1': 'str',
                'opt2': False
            },
            started=datetime.fromisoformat('2000-01-01T12:00:00+00:00'),
            finished=datetime.fromisoformat('2000-01-02T12:00:00+00:00'),
            error='exception'
        )
        log = (
            Log(
                level=LogEnum.debug, message='info message',
                timestamp=datetime.fromisoformat('2000-01-02T00:00:00+00:00')
            ),
            Log(
                level=LogEnum.error, message='error message',
                timestamp=datetime.fromisoformat('2000-01-02T01:00:00+00:00')
            )
        )
        progress = (
            Progress(
                progress={
                    'filename': 'file1.mkv',
                    'extra1': 'foo'
                },
                timestamp=datetime.fromisoformat('2000-01-02T02:00:00+00:00')
            ),
            Progress(
                progress={
                    'filename': 'file2.mkv',
                    'extra2': 'foo'
                },
                timestamp=datetime.fromisoformat('2000-01-02T03:00:00+00:00')
            )
        )

        expected_json = {
            'id': str(self.random_uuid),
            'status': 'finished',
            'urls': ['url1', 'url2', 'url3'],
            'ytdl_opts': {
                'opt1': 'str',
                'opt2': False
            },
            'started': '2000-01-01T12:00:00+00:00',
            'finished': '2000-01-02T12:00:00+00:00',
            'error': 'exception',
            'logs': [
                {
                    'level': 'debug',
                    'message': 'info message',
                    'timestamp': '2000-01-02T00:00:00+00:00'
                },
                {
                    'level': 'error',
                    'message': 'error message',
                    'timestamp': '2000-01-02T01:00:00+00:00'
                }
            ],
            'progress': [
                {
                    'filename': 'file1.mkv',
                    'extra1': 'foo'
                },
                {
                    'filename': 'file2.mkv',
                    'extra2': 'foo'
                }
            ]
        }

        self.patch_job(job)
        self.patch_log(log)
        self.patch_progress(progress)

        response = self.client.get(f'/jobs/{self.random_uuid}')
        self.assertEqual(response.status_code, 200)

        response_json = response.get_json()
        self.assertEqual(response_json, expected_json)
