from __future__ import annotations

from typing import TYPE_CHECKING
import os
import unittest.mock as mock

from .base import TestBase, TestJobCheckPathSeps
from ytdl_server.error import http_error
import ytdl_server.job as job

if TYPE_CHECKING:
    from typing import Any


@mock.patch('os.sep', new='/')
@mock.patch('os.altsep', new=None)
class TestJobCheckPathSepsUnix(TestJobCheckPathSeps.Tests):
    def test_backslash(self) -> None:
        """_check_path_seps - Unix - backslash"""
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_path_seps('name_with\\backslash')

    def test_single_backslash(self) -> None:
        """_check_path_seps - Unix - single backslash"""
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_path_seps('\\')


@mock.patch('os.sep', new='\\')
@mock.patch('os.altsep', new='/')
class TestJobCheckPathSepsWindows(TestJobCheckPathSeps.Tests):
    def test_backslash(self) -> None:
        """_check_path_seps - Windows - backslash"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_path_seps('name_with\\backslash')

    def test_single_backslash(self) -> None:
        """_check_path_seps - Windows - single backslash"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_path_seps('\\')


# TODO: try to disable the mock objects in setUp() and see if it
# improves performance for these
class TestCheckOutTmplDir(TestBase):
    def check_outtmpl_dir(
        self, download_dir: str, outtmpl: Any, force: bool = True
    ) -> None:
        """Run `_check_outtmpl_dir()` with Flask context

        Also sets `download_dir` and `force_download_dir`,
        which are used by the function
        """
        self.setUpApp({
            'download_dir': download_dir,
            'force_download_dir': force
        })
        with self.app.app_context():
            job._check_outtmpl_dir(outtmpl)

    def test_basic_true(self) -> None:
        """_check_outtmpl_dir - basic True"""
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/parent', '/parent/child')

    def test_basic_false(self) -> None:
        """_check_outtmpl_dir - basic False"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/parent', '/different/child')

    def test_exact(self) -> None:
        """_check_outtmpl_dir - same path"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/same', '/same')

    def test_dots_false(self) -> None:
        """_check_outtmpl_dir - dot escape"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/parent', '/parent/child/..')

    def test_dots_true(self) -> None:
        """_check_outtmpl_dir - dots still child"""
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/parent', '/parent/child/child2/..')

    def test_dots_different_dir(self) -> None:
        """_check_outtmpl_dir - dot escape - completely different dir"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/parent', '/parent/../other')

    def test_dots_complicated_dots_within(self) -> None:
        """_check_outtmpl_dir - complicated dots without escaping"""
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir(
                '/parent', '/parent/child1/../child2/./child3/../child2/child1'
            )

    def test_root_dir(self) -> None:
        """_check_outtmpl_dir - root dir"""
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/', '/file')

    def test_root_dir_same(self) -> None:
        """_check_outtmpl_dir - root dir - same path"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/', '/')

    def test_relative_true_dot(self) -> None:
        """_check_outtmpl_dir - relative is in - dot parent dir"""
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('.', 'file')

    def test_relative_true_absolute(self) -> None:
        """_check_outtmpl_dir - relative is in - absolute parent dir"""
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir(os.getcwd(), 'file')

    def test_absolute_relative(self) -> None:
        """_check_outtmpl_dir - absolute parent, relative child"""
        random_dir = os.path.join('/', str(self.random_uuid))
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir(random_dir, 'file')

    def test_empty_string_current_dir_dot(self) -> None:
        """_check_outtmpl_dir - empty string - dot

        Empty child resolves to the current dir, so this should return
        False because the parent and child are equal
        """
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('.', '')

    def test_empty_string_current_dir(self) -> None:
        """_check_outtmpl_dir - empty string - current dir

        Empty child resolves to the current dir, so this should return
        False because the parent and child are equal
        """
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir(os.getcwd(), '')

    def test_doubledot_empty(self) -> None:
        """_check_outtmpl_dir - double dot - empty string"""
        self.setUpParentDir()
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('..', '')

    def test_doubledot_dot(self) -> None:
        """_check_outtmpl_dir - double dot - dot"""
        self.setUpParentDir()
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('..', '.')

    def test_doubledot_doubledot(self) -> None:
        """_check_outtmpl_dir - double dot - double dot"""
        self.setUpParentDir()
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('..', '..')

    def test_dot_doubledot(self) -> None:
        """_check_outtmpl_dir - dot - doubledot"""
        self.setUpParentDir()
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('.', '..')

    def test_doubledot_file(self) -> None:
        """_check_outtmpl_dir - double dot - file"""
        self.setUpParentDir()
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('..', 'file')

    def test_empty_string_random_parent(self) -> None:
        """_check_outtmpl_dir - empty string - current dir"""
        random_dir = os.path.join('/', str(self.random_uuid))
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir(random_dir, '')

    def test_same_dot_dot(self) -> None:
        """_check_outtmpl_dir - same dir - dot dot"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('.', '.')

    def test_same_cwd_dot(self) -> None:
        """_check_outtmpl_dir - same dir - cwd dot"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir(os.getcwd(), '.')

    def test_same_dot_cwd(self) -> None:
        """_check_outtmpl_dir - same dir - dot cwd"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('.', os.getcwd())

    def test_dot_random_parent(self) -> None:
        """_check_outtmpl_dir - dot - random dir"""
        random_dir = os.path.join('/', str(self.random_uuid))
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir(random_dir, '.')

    def test_excessive_slashes_true(self) -> None:
        """_check_outtmpl_dir - excessive number of slashes - True"""
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('//parent////', '/parent///child//')

    def test_excessive_slashes_false(self) -> None:
        """_check_outtmpl_dir - excessive number of slashes - False"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('//different////', '/child//')

    def test_config_disabled(self) -> None:
        """_check_outtmpl_dir - not in parent, but config option is
        disabled"""
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/parent', '/different/child', False)

    def test_wrong_type(self) -> None:
        """_check_outtmpl_dir - Wrong type"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/parent', 1234)

    def test_dict_valid(self) -> None:
        """_check_outtmpl_dir - Dict with valid values"""
        with self.assertDoesNotRaise(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/parent', {
                'absolute': '/parent/child',
                'relative': 'child'
            })

    def test_dict_invalid(self) -> None:
        """_check_outtmpl_dir - Dict with invalid values"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/parent', {
                'ok': '/parent/child',
                'bad': '/other/child'
            })

    def test_dict_wrong_type(self) -> None:
        """_check_outtmpl_dir - Dict that contains the wrong type"""
        with self.assertRaises(http_error.YTDLOptionBadValue):
            self.check_outtmpl_dir('/parent', {
                'not_a_str': 1234
            })
