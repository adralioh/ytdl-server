from __future__ import annotations

import ytdl_server.version as version
from .base import TestBase


class TestMeta(TestBase):
    def test_get(self) -> None:
        """Get /meta"""
        expected_json = {
            'version': version.API
        }

        self.setUpApp()
        response = self.client.get('/meta')

        with self.subTest(msg='Status code'):
            self.assertEqual(200, response.status_code)
        with self.subTest(msg='Content type'):
            self.assertEqual('application/json', response.content_type)
        with self.subTest(msg='Response data'):
            self.assertEqual(expected_json, response.get_json())
