from __future__ import annotations

import unittest
from enum import Enum
from typing import TYPE_CHECKING

import ytdl_server.ytdl as ytdl

if TYPE_CHECKING:
    from typing import Any


class Actions(Enum):
    """Test enum used by the MetadataParser tests

    Based on yt_dlp.MetadataParserPP.Actions:
        https://github.com/yt-dlp/yt-dlp/blob/8b688881ba45ee7a34d45ad33b295adb871f8f5c/yt_dlp/postprocessor/metadataparser.py#L9-L11
    """

    INTERPRET = 'interpretter'
    REPLACE = 'replacer'


class TestYTDLCensorOpts(unittest.TestCase):
    maxDiff = 80*16

    def test_censor_basic(self) -> None:
        """censor_opts - basic"""
        ytdl_opts = {
            'safe1': 'value1',
            'sensitive1': 'value2',
            'sensitive2': 'value3',
            'safe2': {'dict_key': 'dict_value'},
            'sensitive3': [1, 2, 3, 'list_value', 5]
        }
        sensitive_opts = ('sensitive1', 'sensitive2', 'sensitive3')
        censored_opts = {
            'safe1': 'value1',
            'sensitive1': None,
            'sensitive2': None,
            'safe2': {'dict_key': 'dict_value'},
            'sensitive3': None
        }

        result = ytdl.censor_opts(ytdl_opts, sensitive_opts)
        self.assertEqual(result, censored_opts)

    def test_censor_empty_ytdl_opts(self) -> None:
        """censor_opts - ytdl_opts is empty"""
        ytdl_opts: dict[str, Any] = {}
        sensitive_opts = ('sensitive1', 'sensitive2', 'sensitive3')

        result = ytdl.censor_opts(ytdl_opts, sensitive_opts)
        self.assertEqual(result, ytdl_opts)

    def test_censor_empty_sensitive_opts(self) -> None:
        """censor_opts - sensitive_opts is empty"""
        ytdl_opts = {
            'safe1': 'value1',
            'sensitive1': 'value2',
            'sensitive2': 'value3',
            'safe2': {'dict_key': 'dict_value'},
            'sensitive3': [1, 2, 3, 'list_value', 5]
        }
        sensitive_opts: tuple[str, ...] = tuple()

        result = ytdl.censor_opts(ytdl_opts, sensitive_opts)
        self.assertEqual(result, ytdl_opts)

    def test_censor_copy(self) -> None:
        """censor_opts - Don't change input

        Verify that censor_opts returns a new dict instead of
        modifying ytdl_opts in-place
        """
        ytdl_opts = {
            'safe1': 'value1',
            'sensitive1': 'value2',
            'sensitive2': 'value3',
            'safe2': {'dict_key': 'dict_value'},
            'sensitive3': [1, 2, 3, 'list_value', 5]
        }
        sensitive_opts = {'sensitive1', 'sensitive2', 'sensitive3'}
        ytdl_copy = ytdl_opts.copy()

        result = ytdl.censor_opts(ytdl_opts, sensitive_opts)
        self.assertIsNot(result, ytdl_opts)
        self.assertEqual(ytdl_opts, ytdl_copy)
