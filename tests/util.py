"""Utility functions that are used by both unit and functional tests"""

from __future__ import annotations

import urllib.parse
import uuid
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any, Union


def assertIsUUID(str_: Any) -> None:
    """Assert that the given string is a valid UUID"""
    if not isinstance(str_, str):
        raise AssertionError(
            f'uuid should be a string. actual type: {type(str_).__name__}. '
            f'value: {str_}'
        )

    try:
        uuid.UUID(str_)
    except ValueError as e:
        raise AssertionError(f'not a valid UUID: {str_}') from e


def assertValidStatusURL(job_id: Union[uuid.UUID, str], url: str) -> None:
    """Assert that the given URL is a valid job status redirect URL

    Example: http://localhost:5000/jobs/<job_id>
    """
    status_path = urllib.parse.urlparse(url).path
    if status_path != f'/jobs/{job_id}':
        raise AssertionError(f'invalid status URL: {url}')
