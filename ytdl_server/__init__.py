"""Remotely download videos using youtube-dl via a REST API"""

# Ignore unused-import linting error
from .version import MAIN as __version__  # noqa: F401
